package com.mini.trading.adapters;

import quickfix.Acceptor;

import com.ohua.engine.resource.management.AbstractConnection;
import com.mini.trading.adapters.FIXEventDispatch;
import com.mini.trading.adapters.FIXResourceConnection;

public class FIXConnection extends AbstractConnection {

  private Acceptor _acceptor = null;
  private FIXEventDispatch _application = null;
  
  protected FIXConnection(Acceptor acceptor, FIXEventDispatch application) {
   _acceptor = acceptor;
   _application = application;
  }
  
  @Override
  protected void close() throws Throwable {
    _acceptor.stop();
  }
  
  @Override
  protected FIXResourceConnection getRestrictedConnection() {
    return new FIXResourceConnection(super.getConnectionID(), _application);
  }
  
  @Override
  protected void commit() throws Throwable {
    throw new UnsupportedOperationException("Currently transactions are not supported for FIX connections.");
  }
  
  @Override
  protected void rollback() throws Throwable {
    throw new UnsupportedOperationException("Currently transactions are not supported for FIX connections.");
  }
  
}
