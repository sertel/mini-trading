package com.mini.trading.adapters;

import quickfix.FieldNotFound;
import quickfix.IncorrectTagValue;
import quickfix.Message;
import quickfix.MessageCracker;
import quickfix.SessionID;
import quickfix.UnsupportedMessageType;
import quickfix.fix50.RFQRequest;

import com.ohua.lang.Function;
import com.mini.trading.adapters.FIXResourceConnection;

/**
 * Function implementations for retrieving, parsing, creating and sending FIX messages.
 * 
 * @author sertel
 * 
 */
public abstract class Adapters {
  
  public static class FIXAccepter {
    @Function
    public Object[] acceptFix(FIXResourceConnection cnn) {
      return cnn.receive();
    }
  }
  
  public static class FIXSender {
    @Function
    public Object[] sendFix(Message msg, FIXResourceConnection cnn) {
      boolean result = cnn.send(msg);
      return new Object[] { result };
    }
  }
  
  public static class FIXParser extends MessageCracker {
    
    private Object[] _result = null;
    
    @Function
    public Object[] parseFix(Message msg, SessionID id){
      _result = null;
      
      if(msg.isAdmin()){
        // TODO
        return null;
      }else if(msg.isApp()){
        try {
          super.crack(msg, id);
        }
        catch(UnsupportedMessageType | FieldNotFound | IncorrectTagValue e) {
          // TODO throw proper exception here!
          e.printStackTrace();
          throw new RuntimeException(e);
        }
        return _result;
      }else{
        assert false;
        throw new RuntimeException("impossible");
      }
    }
    
    @Handler
    protected void onMessage(RFQRequest msg, SessionID id){
      // TODO convert this message into mini.trading format
    }
  }
  
  public static class FIXSerializer {
    @Function
    public Object[] serializeFix(){
      // TODO
      return null;
    }
  }
}
