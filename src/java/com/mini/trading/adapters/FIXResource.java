package com.mini.trading.adapters;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Map;

import quickfix.Acceptor;
import quickfix.ConfigError;
import quickfix.DefaultMessageFactory;
import quickfix.FileLogFactory;
import quickfix.FileStoreFactory;
import quickfix.LogFactory;
import quickfix.MessageFactory;
import quickfix.MessageStoreFactory;
import quickfix.RuntimeError;
import quickfix.SessionSettings;
import quickfix.SocketAcceptor;

import com.ohua.engine.AbstractExternalActivator;
import com.ohua.engine.AbstractExternalActivator.ManagerProxy;
import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.engine.resource.management.AbstractConnection;
import com.ohua.engine.resource.management.AbstractResource;
import com.ohua.engine.resource.management.ResourceAccess;
import com.mini.trading.adapters.FIXConnection;
import com.mini.trading.adapters.FIXEventDispatch;

public class FIXResource extends AbstractResource {
  
  public FIXResource(ManagerProxy arg0, Map<String, String> arg1) {
    super(arg0, arg1);
  }
  
  @Override
  protected AbstractConnection getConnection(Object... args) {
    
    // create all FIX stuff needed here and then a connection resembles to an application in FIX
    // terminology
    try {
      String fileName = (String) args[0];
      SessionSettings settings = new SessionSettings(new FileInputStream(fileName));
      MessageStoreFactory storeFactory = new FileStoreFactory(settings);
      LogFactory logFactory = new FileLogFactory(settings);
      MessageFactory messageFactory = new DefaultMessageFactory();
      FIXEventDispatch application = new FIXEventDispatch();
      Acceptor acceptor = new SocketAcceptor(application, storeFactory, settings, logFactory, messageFactory);
      acceptor.start();
      FIXConnection cnn = new FIXConnection(acceptor, application);
      return cnn;
    }
    catch(ConfigError | FileNotFoundException ce) {
      throw new RuntimeError(ce);
    }
  }
  
  /**
   * We do not provide one as we implement blocking IO inside the incoming connections.
   */
  @Override
  protected AbstractExternalActivator getExternalActivator(OperatorID arg0) {
    return null;
  }
  
  @Override
  public void validate() {
    // TODO validation of the file name can be done here.
  }

  @Override
  public ResourceAccess getResourceAccess(OperatorID arg0, String arg1) {
    // TODO Auto-generated method stub
    return null;
  }
  
}
