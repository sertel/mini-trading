package com.mini.trading.logs;

import java.util.HashMap;
import java.util.Map;

import com.mini.trading.logs.RfsLog;

public class RfsLog {

	// TODO make the log persistent
	
	private Map<String,String> rfsToProduct = new HashMap<String,String>();
	private static RfsLog log = new RfsLog();
	
	public static RfsLog getInstance() {
		return log;
	}
	
	public void addRfs(String rfsId, String product) {
		rfsToProduct.put(rfsId, product);
	}
	
	public String getProduct(String rfsId) {
		return rfsToProduct.get(rfsId);
	}
}