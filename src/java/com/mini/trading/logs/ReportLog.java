package com.mini.trading.logs;

import java.util.ArrayList;
import java.util.List;

import com.mini.trading.logs.ReportLog;

public class ReportLog {

	// TODO make the log persistent
	
	private List<List<String>> reportLog = new ArrayList<List<String>>();
	private static ReportLog log = new ReportLog();
	
	public static ReportLog getInstance() {
		return log;
	}
	
	public void addRfs(List<String> report) {
		reportLog.add(report);
	}
	
	public List<List<String>> getReports() {
		return reportLog;
	}
}