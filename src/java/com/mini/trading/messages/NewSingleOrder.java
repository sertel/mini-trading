package com.mini.trading.messages;

public enum NewSingleOrder {
  PROVIDER,
  ORDER_ID, // string
  QUOTE_ID, // string
  ACCOUNT, // string
  USERNAME, // string
  SENDING_TIME, // java.util.Date
  PRODUCT_TYPE, // string
  IS_REQUESTER_BUYING,
  PRICE_LIMIT,
  PRICE, // double
  SYMBOL, // string
  CURRENCY, // String
  COUPON_RATE, // double
  COMMISSION, // double
  COVERED_OR_UNCOVERED, // int or boolean
  ORDER_QTY, // double
  ORDER_RESTRICTION, // String
  ORDER_TYPE, // char
  TRANSACT_TIME, // java.util.Date
  SIDE, // char
  ORDER_QTY_2 // double -> optional
}
