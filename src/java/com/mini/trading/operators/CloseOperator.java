package com.mini.trading.operators;

import java.io.IOException;
import java.net.Socket;

import com.ohua.lang.Function;
import com.mini.trading.operators.LogFunctionality;

public class CloseOperator {

	@Function
	public Object[] close(Socket socket) {
		try {
		  if(LogFunctionality.DEBUG)
		    System.out.println("(close): closing socket ...");
			socket.close();
			if(LogFunctionality.DEBUG)
			  System.out.println("(close): closed socket " + socket);
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return new Object[]{};
	}
}
