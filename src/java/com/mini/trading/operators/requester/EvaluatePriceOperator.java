package com.mini.trading.operators.requester;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mini.trading.operators.LogFunctionality;
import com.ohua.lang.Function;
import com.ohua.lang.compile.analysis.qual.ReadOnly;

public class EvaluatePriceOperator {

	@Function
	public Object[] evalPrice (@ReadOnly List<String> price) {

		String priceId = "";
		String rfsId = "";
		String priceTime;
		double buyprice = 0.0d;
		double sellprice = 0.0d;
		String provider = "";

		boolean buy = false;
		
		for (String requestLine : price) {
			String[] keyValue = requestLine.split("=");
			switch (keyValue[0]) {
			case "type" : if (!keyValue[1].equals("price")) return null; //throw new RuntimeException("Message is of the wrong type.");
			break;
			case "id" :	priceId = keyValue[1];
			break;
			case "time": priceTime = keyValue[1];
			break;
			case "rfsid": rfsId = keyValue[1];
			break;
			case "buyprice": buyprice = Double.parseDouble(keyValue[1]);
			break;
			case "sellprice": sellprice = Double.parseDouble(keyValue[1]);
			break;
			case "provider": provider = keyValue[1];
			break;
			default: break;
			}
		}
		if (buyprice != 0 && sellprice!= 0 && !rfsId.equals("")) {
			buy = decide(rfsId, buyprice, sellprice); 
		}
		if (buy) {
			List<String> orderInfo = new ArrayList<String>();
			orderInfo.add("rfsId=" + rfsId);
			String amount = String.valueOf(sellprice);
			orderInfo.add("price=" + amount);
			orderInfo.add("provider=" + provider);
			
			if(LogFunctionality.DEBUG)
			  System.out.println("(evaluate-price): buying rfs " + rfsId + " for the price " + amount + " from " + provider);
			return new Object[] {"buy", orderInfo};
		} else {
		  if(LogFunctionality.DEBUG)
		    System.out.println("(evaluate-price): rejecting offer " + Arrays.deepToString(price.toArray()));
			return new Object[] {"reject", new ArrayList<String>()};
		}
	}

	private boolean decide(String rfsId, double buyprice, double sellprice) {
		// TODO decide whether to buy or not
		return true;
	}

}