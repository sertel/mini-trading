package com.mini.trading.operators.requester;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import com.mini.trading.logs.RfsLog;
import com.mini.trading.operators.LogFunctionality;
import com.ohua.lang.Function;

public class CreateRfsOperator {

	private boolean executed = false;
	
	@Function
	public Object[] createRfs(String requester, String product) {
		if (executed) return null;
		executed = true;
		
		long timeInMillis = GregorianCalendar.getInstance().getTimeInMillis();
		Timestamp time = new Timestamp(timeInMillis);
		String id = getId(requester);
		
		List<String> rfs = new ArrayList<String>();
		rfs.add("type=rfs");
		rfs.add("id=" + id);
		rfs.add("time=" + time);
		rfs.add("product=" + product);
		rfs.add("requester=" + requester);
		
		RfsLog log = RfsLog.getInstance();
		log.addRfs(id, product);
		
		if(LogFunctionality.DEBUG)
		  System.out.println("(create-rfs): created a RFS for " + product + " from " + requester);
		return new Object[] {rfs};
	}
	
	
	private String getId(String requester) {
		// TODO
		Random rnd = new Random(100000);
		String id = String.valueOf(rnd.nextLong());
		return requester + id;
	}
	
}
