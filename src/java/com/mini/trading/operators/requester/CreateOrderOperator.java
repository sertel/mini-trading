package com.mini.trading.operators.requester;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import com.mini.trading.logs.RfsLog;
import com.mini.trading.operators.LogFunctionality;
import com.ohua.lang.Function;

public class CreateOrderOperator {

	@Function
	public Object[] createOrder(List<String> orderInfo, String requester) {

		String rfsId = "", price = "", provider = "";

		for (String orderLine : orderInfo) {
			String[] keyValue = orderLine.split("=");
			switch (keyValue[0]) {
			case "rfsId": rfsId = keyValue[1];
			break;
			case "price": price = keyValue[1];
			break;
			case "provider": provider = keyValue[1];
			break;
			default: break;
			}
		}

		long timeInMillis = GregorianCalendar.getInstance().getTimeInMillis();
		String id = getOrderId(rfsId);
		RfsLog rfsLog = RfsLog.getInstance();
		Timestamp time = new Timestamp(timeInMillis);
		String product = "";

		List<String> order = new ArrayList<String>();
		order.add("type=order");
		order.add("id=" + id);
		order.add("time=" + time);
		order.add("rfsid=" + rfsId);
		if ((product = rfsLog.getProduct(rfsId)) != null) { 
			order.add("product=" + product);
		} else {
			order.add("product=" + "unknown");
		}
		order.add("isrequesterbuying=true");
		order.add("pricelimit=" + price);
		order.add("requester=" + requester);
		order.add("provider=" + provider);

		if(LogFunctionality.DEBUG)
		  System.out.println("(create-order): returning "  + Arrays.deepToString(order.toArray()));
		return new Object[] {order};
	}

	private String getOrderId(String rfsId) {
		// TODO
		Random rnd = new Random(100000);
		String id = String.valueOf(rnd.nextLong());
		return rfsId + id;
	}

}
