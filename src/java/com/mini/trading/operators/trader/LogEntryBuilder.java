package com.mini.trading.operators.trader;

import com.ohua.lang.Function;

public abstract class LogEntryBuilder
{
  
  public static class AdminRequestLogEntryBuilder
  {
    @Function
    public Object[] buildAdminLogEntry(boolean tradingAllowed) {
      return new Object[] { Boolean.toString(tradingAllowed) };
    }
  }
  
}
