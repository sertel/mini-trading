package com.mini.trading.operators.trader;

import java.util.Map;

import com.mini.trading.operators.LogFunctionality;
import com.ohua.lang.Function;

public class RegisterOperator
{
  @Function
  public Object[] register(String provider, String address, String port, Map<String, String[]> provs) {
    String[] value = new String[] { address,
                                   port };
    provs.put(provider, value);
    if(LogFunctionality.DEBUG)
      System.out.println("(register): registered " + provider + ", " + address + ":" + port);
    return new Object[] {};
  }
}
