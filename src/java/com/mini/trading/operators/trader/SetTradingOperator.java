package com.mini.trading.operators.trader;

import java.util.concurrent.atomic.AtomicReference;

import com.mini.trading.operators.LogFunctionality;
import com.ohua.lang.Function;

public class SetTradingOperator
{
  
  @Function
  public Object[] setTrading(boolean newTrading, AtomicReference<Boolean> oldTrading) {
    oldTrading.set(newTrading);
    if(LogFunctionality.DEBUG)
      System.out.println("(set-trading): set trading to " + newTrading);
    return new Object[] { newTrading };
  }
  
}
