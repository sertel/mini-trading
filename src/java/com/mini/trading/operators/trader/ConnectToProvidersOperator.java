package com.mini.trading.operators.trader;

import java.io.IOException;
import java.net.Socket;

import com.mini.trading.operators.LogFunctionality;
import com.ohua.lang.Function;

public class ConnectToProvidersOperator {
  @Function
  public Object[] connectToProv(String addr, String port) {
    try {
      System.out.println("Trying to connect to provider: " + addr + ":" + port);
      Socket socket = new Socket(addr, new Integer(port));
      if(LogFunctionality.DEBUG) 
        System.out.println("(connect-to-provs): connected to " + addr);
      return new Object[] { socket };
    }
    catch(IOException e) {
      System.out.println("Connect to provider " + addr + ":" + port + "failed with: " + e.getMessage());
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
}
