package com.mini.trading.operators.trader;

import java.util.ArrayList;
import java.util.List;

import com.mini.trading.operators.LogFunctionality;
import com.ohua.lang.Function;

public class RFSFunctionality
{
  public static class RFSMessageBuilder
  {
    @Function
    public Object[] buildRfsMsg(String id, String time, String product, String requester) {
      List<String> l = new ArrayList<>();
      l.add("type=rfs");
      l.add("id=" + id);
      l.add("time=" + time);
      l.add("product=" + product);
      l.add("requester=" + requester);
      if(LogFunctionality.DEBUG)
        System.out.println("(build-rfs-message): RFS message build!");
      return new Object[] { l };
    }
  }
  
}
