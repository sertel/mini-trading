package com.mini.trading.operators.trader;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mini.trading.messages.NewSingleOrder;
import com.mini.trading.operators.LogFunctionality;
import com.ohua.lang.Function;
import com.ohua.lang.compile.analysis.qual.ReadOnly;

public abstract class TraderRequestHandling {
  
  public static class AdminRequestParser {
    @Function
    public Object[] parseAdminRequest(List<String> request) {
      boolean trading = false;
      for(String line : request) {
        String[] keyValue = line.split("=");
        if(keyValue[0].equalsIgnoreCase("trading")) {
          if(keyValue[1].equalsIgnoreCase("enabled")) {
            trading = true;
            break;
          }
        }
      }
      if(LogFunctionality.DEBUG) System.out.println("(parse-admin-request): Admin request parsed!");
      return new Boolean[] { trading };
    }
  }
  
  public static class RegisterRequestParser {
    @Function
    public Object[] parseRegisterRequest(List<String> request) {
      String provider = "";
      String addr = "";
      String port = "";
      
      for(String line : request) {
        String[] keyValue = line.split("=");
        switch(keyValue[0]) {
          case "provider":
            provider = keyValue[1];
            break;
          case "addr":
            addr = keyValue[1];
            break;
          case "port":
            port = keyValue[1];
            break;
          default:
            System.out.println("(parse-register-request): WARNING: Received unknown data: "
                               + Arrays.deepToString(request.toArray()));
            break;
        }
      }
      if(LogFunctionality.DEBUG) System.out.println("(parse-register-request): Parsed register request: "
                                                    + provider + " " + addr + " " + port);
      return new String[] { provider,
                           addr,
                           port };
    }
  }
  
  public static class OrderRequestParser {
    @Function
    public Object[] parseOrderRequest(@ReadOnly List<String> request) {
      String provider = null;
      Map<NewSingleOrder, String> m = new HashMap<>();
      NewSingleOrder[] enums = NewSingleOrder.values();
      for(String requestLine : request) {
        String[] keyValue = requestLine.split("=");
        
        NewSingleOrder key = enums[Integer.parseInt(keyValue[0])];
        if(key == NewSingleOrder.PROVIDER) provider = keyValue[1];
        else m.put(key, keyValue[1]);
      }
      if(LogFunctionality.DEBUG)
        System.out.println("(parse-order-request): Order request parsed!");
      return new Object[] { provider,
                           m };
    }
  }
  
  public static class OrderRequestParserOld {
    @Function
    public Object[] parseOrderRequestOld(@ReadOnly List<String> request) {
      String provider = null;
      Map<String, String> m = new HashMap<>();
      for(String requestLine : request) {
        String[] keyValue = requestLine.split("=");
        
        switch(keyValue[0]) {
          case "id":
          case "time":
          case "rfsid":
          case "product":
          case "isrequesterbuying":
          case "pricelimit":
          case "requester":
            m.put(keyValue[0], keyValue[1]);
            break;
          case "provider":
            provider = keyValue[1];
            break;
          default:
            System.out.println("(parse-order-request): WARNING: Received unknown request data: "
                               + Arrays.deepToString(request.toArray()));
            break;
        }
      }
      if(LogFunctionality.DEBUG)
        System.out.println("(parse-order-request): Order request parsed!");
      return new Object[] { provider,
                           m };
    }
  }
  
  
  public static class PriceResponseParser {
    @Function
    public Object[] parsePriceInfo(@ReadOnly List<String> msg) {
      String id = null;
      String time = null;
      String rfsID = null;
      String buyPrice = null;
      String sellPrice = null;
      String provider = null;
      
      for(String requestLine : msg) {
        String[] keyValue = requestLine.split("=");
        
        switch(keyValue[0]) {
          case "price":
            break;
          case "id":
            id = keyValue[1];
            break;
          case "time":
            time = keyValue[1];
            break;
          case "rfsid":
            rfsID = keyValue[1];
            break;
          case "buyprice":
            buyPrice = keyValue[1];
            break;
          case "sellprice":
            sellPrice = keyValue[1];
            break;
          case "provider":
            provider = keyValue[1];
            break;
          case "type":
            break;
          default:
            System.out.println("(parse-price-info): WARNING:Received unknown request data: "
                               + Arrays.deepToString(msg.toArray()));
            break;
        }
      }
      return new String[] { id,
                           time,
                           rfsID,
                           buyPrice,
                           sellPrice,
                           provider };
    }
  }
  
  
  public static class UnknownRequestError{
    @Function
    public Object[] reportUnknownRequest(String requestType){
      System.out.println("Unknown request type: " + requestType);
      return new Object[0];
    }
  }
}
