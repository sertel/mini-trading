package com.mini.trading.operators.trader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import aua.analysis.qual.Untainted;

import com.mini.trading.messages.NewSingleOrder;
import com.mini.trading.operators.LogFunctionality;
import com.ohua.lang.Function;
import com.ohua.lang.compile.analysis.qual.ReadOnly;

public class OrderArgsOperator
{
  
  public static class ProviderRegistryAccess
  {
    @Function
    public Object[] getProvider(String provider, Map<String, String[]> provs) {
      @Untainted String[] info = provs.get(provider);
      // FIXME this is a check on the algorithm level as it influences processing of the result!
      if(info == null){
        System.err.println("Provider not found: " + provider + ".");
        System.err.println("Available providers: " + Arrays.deepToString(provs.keySet().toArray()));
      }
//      System.out.println("(get-provider): Provider info: " + Arrays.deepToString(info));
      return new Object[] { info[0],
                           info[1] };
    }
  }
  
  public static class OrderMessageBuilder
  {
    @Function
    public Object[] buildOrderMsg(@ReadOnly Map<NewSingleOrder, ?> order) {
      List<String> msg = new ArrayList<>();
      msg.add("type=order");
      for(Map.Entry<NewSingleOrder, ?> entry : order.entrySet()) {
        msg.add(entry.getKey().ordinal() + "=" + entry.getValue());
      }
      if(LogFunctionality.DEBUG)
        System.out.println("(build-order-msg): Constructed order message!");
      return new Object[] { msg };
    }
  }
  
  public static class OrderMessageBuilderOld
  {
    @Function
    public Object[] buildOrderMsgOld(@ReadOnly Map<String, String> order) {
      List<String> msg = new ArrayList<>();
      msg.add("type=order");
      for(Map.Entry<String, String> entry : order.entrySet()) {
        msg.add(entry.getKey() + "=" + entry.getValue());
      }
      if(LogFunctionality.DEBUG)
        System.out.println("(build-order-msg): Constructed order message!");
      return new Object[] { msg };
    }
  }
}
