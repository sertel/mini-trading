package com.mini.trading.operators.trader;

import java.util.concurrent.atomic.AtomicReference;

import com.mini.trading.operators.LogFunctionality;
import com.ohua.lang.Function;

public class TradingAllowedOperator
{
  
  @Function
  public Object[] tradingAllowed(Object o, AtomicReference<Boolean> trading) {
    boolean trad = trading.get();
	  if (trad) {
	    if(LogFunctionality.DEBUG)
	      System.out.println("(trading-allowed): Trading is allowed.");
	  } else {
	    if(LogFunctionality.DEBUG)
	      System.out.println("(trading-allowed): Trading is not allowed.");
	  }
    return new Object[] { trad };
  }
  
}
