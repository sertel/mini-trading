package com.mini.trading.operators.provider;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.mini.trading.messages.ExecutionReport;
import com.mini.trading.operators.LogFunctionality;
import com.mini.trading.operators.provider.ProviderFunctionality.ProductDetails;
import com.ohua.lang.Function;

public abstract class MessageBuilder {
  
  public static class PriceMessageBuilder {
    @Function
    public Object[] buildPriceMsg(Timestamp time, ProductDetails details, String rfsId, String provider) {
//      public Object[] buildPriceMsg(Timestamp time, String rfsId, String provider, ProductDetails details) {
      List<String> msg = new ArrayList<>();
      
      msg.add("type=price");
      msg.add("id=" + details._id);
      msg.add("time=" + time);
      msg.add("rfsid=" + rfsId);
      msg.add("buyprice=" + details._buyPrice);
      msg.add("sellprice=" + details._sellPrice);
      msg.add("provider=" + provider);
      if(LogFunctionality.DEBUG) 
        System.out.println("(build-price-msg): Price message built!");
      
      return new Object[] { msg };
    }
  }
  
  private static abstract class ReportMessageBuilder {
    public List<String> buildReportMessage(String orderId, String requester, String provider) {
      long timeInMillis = GregorianCalendar.getInstance().getTimeInMillis();
      Timestamp time = new Timestamp(timeInMillis);
      List<String> msg = new ArrayList<String>();
      // String id = ProviderFunctionality.getId(provider);
      
      msg.add("type=report");
      msg.add(ExecutionReport.EXEC_ID.ordinal() + "=" + "Z4234232323");
      msg.add(ExecutionReport.SENDING_TIME.ordinal() + "=" + time);
      msg.add(ExecutionReport.SETTL_DATE.ordinal() + "=" + time);
      msg.add(ExecutionReport.ORDER_ID.ordinal() + "=" + orderId);
      msg.add(ExecutionReport.AVG_PX.ordinal() + "=" + 0.7945);
      msg.add(ExecutionReport.ACCOUNT.ordinal() + "=" + requester);
      msg.add("provider=" + provider);
      
      // TODO this is dummy data. the algorithm needs to changed in order to take this
      // information from somewhere!
      msg.add(ExecutionReport.ORDER_QTY.ordinal() + "=" + 200000);
      msg.add(ExecutionReport.TRANSACT_TIME.ordinal() + "=" + time);
      msg.add(ExecutionReport.LAST_PX.ordinal() + "=" + 0.783573);
      msg.add(ExecutionReport.LEAVES_QTY.ordinal() + "=" + 0);
      msg.add(ExecutionReport.PRODUCT_TYPE.ordinal() + "=" + "FX-STD");
      msg.add(ExecutionReport.CURRENCY.ordinal() + "=" + "USD");
      msg.add(ExecutionReport.SYMBOL.ordinal() + "=" + "NZD/USD");
      msg.add(ExecutionReport.LAST_PX.ordinal() + "=" + 0.75);
      msg.add(ExecutionReport.SIDE.ordinal() + "=" + "F");
      
      return msg;
    }
  }
  
  public static class PositiveReportMessageBuilder extends ReportMessageBuilder {
    @Function
    public Object[] buildPositiveReportMsg(String orderId, double price, String requester, String provider) {
      List<String> msg = super.buildReportMessage(orderId, requester, provider);
      msg.add(ExecutionReport.EXEC_TYPE.name() + "=" + 2);
      msg.add("executedprice=" + price);
      if(LogFunctionality.DEBUG) System.out.println("(build-positive-report-msg): Positive report message built!");
      return new Object[] { msg };
    }
  }
  
  public static class RejectionReportMessageBuilder extends ReportMessageBuilder {
    @Function
    public Object[] buildRejectionReportMsg(String orderId, String requester, String provider) {
      List<String> msg = super.buildReportMessage(orderId, requester, provider);
      msg.add(ExecutionReport.EXEC_TYPE.name() + "=" + 8);
      
      if(LogFunctionality.DEBUG) System.out.println("(build-rejection-report-message): Rejection report message built!");
      
      return new Object[] { msg };
    }
  }
  
}
