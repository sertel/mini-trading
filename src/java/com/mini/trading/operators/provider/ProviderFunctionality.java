package com.mini.trading.operators.provider;

import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.Map;

import aua.analysis.qual.Untainted;

import com.mini.trading.operators.LogFunctionality;
import com.ohua.lang.Function;

public abstract class ProviderFunctionality {
  
  public static class ProductDetails {
    int _id = -1;
    double _buyPrice = 0.0;
    double _sellPrice = 0.0;
    
    public ProductDetails(int id, double buyPrice, double sellPrice) {
      _id = id;
      _buyPrice = buyPrice;
      _sellPrice = sellPrice;
    }
  }
  
  public static class PriceCreator {
    
    @Function
    public Object[] productLookup(String product, Map<String, ProductDetails> products) {
      long timeInMillis = GregorianCalendar.getInstance().getTimeInMillis();
      Timestamp time = new Timestamp(timeInMillis);
      @Untainted ProductDetails details = products.get(product);
      if(LogFunctionality.DEBUG) 
        System.out.println("(create-price): got price " //+ details._sellPrice
                                                    + " for prdocut " + product);
      
      // FIXME the STM rewrite tries to rewrite also the product details class (which is
      // essentially correct because all data structures need to be immutable) but fails doing
      // so. not sure yet whether we should support this because that means we really have to
      // turn all data structures inside the other ops that actually insert into this data
      // structure into immutable types. feels to me like we want to flag that we can not
      // support this and the developer must either go ahead and rewrite the op or just can't
      // leverage the STM rewrite. HOWEVER, what is definitely a bug, is that we fail
      // compilation because of an NPE!
      return new Object[] { time,
                           details };
    }
  }
  
  public static class OrderHandler {
    
    @Function
    public Object[] processOrder(String product, double priceLimit, boolean isRequesterBuying,
                                 ProductDetails details)
    {
      if(!isRequesterBuying) {
        if(LogFunctionality.DEBUG) System.out.println("(process-order): rejected order for product " + product
                                                      + ".");
        return new Object[] { false,
                             "RequesterNotBuying" };
      }
      
      boolean sold = false;
      double sellPrice = details._sellPrice;
      
      // FIXME this should not be performed at the operator level but at the algorithm level
      // because it results in two different return types!
      if(sellPrice >= Double.valueOf(priceLimit)) {
        // TODO sell(product, sellPrice);
        sold = true;
      }
      
      if(sold) {
        if(LogFunctionality.DEBUG) System.out.println("(process-order): sold product " + product + " for "
                                                      + sellPrice + ".");
        return new Object[] { true,
                             sellPrice };
      } else {
        if(LogFunctionality.DEBUG) System.out.println("(process-order): rejected order for product " + product
                                                      + ".");
        return new Object[] { false,
                             "PriceHigherThanLimit" };
      }
    }
  }
}
