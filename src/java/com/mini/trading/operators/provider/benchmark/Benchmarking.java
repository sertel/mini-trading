package com.mini.trading.operators.provider.benchmark;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.CharBuffer;

import aua.analysis.qual.Linear;

import com.ohua.lang.Function;

public abstract class Benchmarking {
  
  public static class Input {
    @Linear private int _idx = 0;
    
    @Function
    public Object[] microBench(Object[] data) {
      if(_idx < data.length) return (Object[]) data[_idx++];
      else return null;
    }
  }
  
  public static class CompoundInput {
    @Linear private int _idx = 0;
    
    @Function
    public Object[] microInput(Object[] data) {
      if(_idx < data.length) return new Object[] { data[_idx++] };
      else return null;
    }
  }
  
  public static class Read {
    @Function
    public Object[] microRead(Object d) {
      return (Object[]) d;
    }
  }
  
  public static class Output extends Socket {
    private CharBuffer bb = CharBuffer.allocate(300);
    public String _output = null;
    
    public boolean isClosed() {
      return false;
    }
    
    public void close() {
      _output = new String(bb.array());
//       System.out.println("Closed! " + this + " size remaining: " + bb.remaining());
    }
    
    public OutputStream getOutputStream() {
      return new OutputStream() {
        
        @Override
        public void write(int b) throws IOException {
//          System.out.println("Writing to " + this + " size remaining: " + bb.remaining());
          bb.put((char) b);
        }
      };
    }
  }
}
