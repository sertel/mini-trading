package com.mini.trading.operators.provider;

import java.util.Arrays;
import java.util.List;

import com.mini.trading.messages.NewSingleOrder;
import com.mini.trading.operators.LogFunctionality;
import com.ohua.lang.Function;

public abstract class ProviderRequestHandling {
  public static class OrderRequestParser
  {
    @Function
    public Object[] parseProviderOrderRequest(List<String> request) {
      String id = null;
      String time = null;
      String rfsId = null;
      String product = null;
      boolean isRequesterBuying = false;
      double priceLimit = 0.0d;
      String requester = null; 
      NewSingleOrder[] vals = NewSingleOrder.values();
      for(String requestLine : request) {
        String[] keyValue = requestLine.split("=");
        
        NewSingleOrder key = vals[Integer.parseInt(keyValue[0])];
        
        switch(key)
        {
          case ORDER_ID:
            id = keyValue[1];
            break;
          case SENDING_TIME:
            time = keyValue[1];
            break;
          case QUOTE_ID:
            rfsId = keyValue[1];
            break;
          case PRODUCT_TYPE:
            product = keyValue[1];
            break;
          case IS_REQUESTER_BUYING:
            isRequesterBuying = keyValue[1].equals("true");
            break;
          case PRICE_LIMIT:
            priceLimit = new Double(keyValue[1]);
            break;
          case ACCOUNT:
            requester =  keyValue[1];
            break;
          default:
//            System.out.println("(parse-order-request): WARNING: Received unknown request data: "
//                + Arrays.deepToString(keyValue));
            break;
        }
      }
      if(LogFunctionality.DEBUG)
        System.out.println("(parse-order-request): Order request parsed!");
      return new Object[] { id, time, rfsId, product, isRequesterBuying, priceLimit, requester };
    }
  }
  
  public static class OrderRequestParserOld
  {
    @Function
    public Object[] parseProviderOrderRequestOld(List<String> request) {
      String id = null;
      String time = null;
      String rfsId = null;
      String product = null;
      boolean isRequesterBuying = false;
      double priceLimit = 0.0d;
      String requester = null; 
      
      for(String requestLine : request) {
        String[] keyValue = requestLine.split("=");
        
        switch(keyValue[0])
        {
          case "id":
            id = keyValue[1];
            break;
          case "time":
            time = keyValue[1];
            break;
          case "rfsid":
            rfsId = keyValue[1];
            break;
          case "product":
            product = keyValue[1];
            break;
          case "isrequesterbuying":
            isRequesterBuying = keyValue[1].equals("true");
            break;
          case "pricelimit":
            priceLimit = new Double(keyValue[1]);
            break;
          case "requester":
            requester =  keyValue[1];
            break;
          default:
            System.out.println("(parse-order-request): WARNING: Received unknown request data: "
                + Arrays.deepToString(request.toArray()));
            break;
        }
      }
      if(LogFunctionality.DEBUG)
        System.out.println("(parse-order-request): Order request parsed!");
      return new Object[] { id, time, rfsId, product, isRequesterBuying, priceLimit, requester };
    }
  }
}
