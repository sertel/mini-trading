package com.mini.trading.operators.provider;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.mini.trading.operators.provider.ProviderFunctionality.ProductDetails;
import com.ohua.lang.Function;

public abstract class PortfolioHandling {
  
  public static class PutParser {
    @Function
    public Object[] parsePut(List<String> content) {
      assert content.size() == 1;
      String[] kvPair = content.get(0).split("=");
      String[] details = kvPair[1].split(":");
      assert details.length == 3;
      return new Object[] { kvPair[0],
                           new ProductDetails(Integer.parseInt(details[0]),
                                              Double.parseDouble(details[1]),
                                              Double.parseDouble(details[2])) };
    }
  }
  
  public static class Updater {
    @Function
    public Object[] updatePortfolio(String key, ProductDetails value, Map<String, ProductDetails> products) {
      ProductDetails old = products.put(key, value);
      return new Object[] { old };
      // FIXME also not handled by our STM rewrite. it fails during execution here!
      // return new Object[] { old == null ? new ProductDetails(-1, -1.0, -1.0) : old };
    }
  }
  
  public static class PutMsg {
    @Function
    public Object[] buildPutMsg(ProductDetails value) {
      return new Object[] { Collections.singletonList(value._id + "=" + value._buyPrice + ":" + value._sellPrice) };
    }
  }
}
