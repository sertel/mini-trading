package com.mini.trading.operators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ohua.lang.Function;

public abstract class RequestHandling {
  
  public static class RequestTypeParser {
    @Function
    public Object[] parseRequestType(List<String> request) {
      System.out.println(request.toString());
      assert request.get(0).startsWith("type");
      String[] s = request.get(0).split("=");
      assert s.length == 2;
      if(LogFunctionality.DEBUG) System.out.println("(parse-request-type): Parsed request type! -> " + s[1]);
      return new Object[] { s[1],
                           new ArrayList<String>(request.subList(1, request.size())) };
    }
  }
  
  public static class RFSRequestParser {
    @Function
    public Object[] parseRfsRequest(List<String> request) {
      System.out.println("RFS request parsing ...");
      String provider = null;
      String id = null;
      String time = null;
      String product = null;
      String requester = null;
      
      for(String requestLine : request) {
        String[] keyValue = requestLine.split("=");
        switch(keyValue[0]) {
          case "id":
            id = keyValue[1];
            break;
          case "time":
            time = keyValue[1];
            break;
          case "product":
            product = keyValue[1];
            break;
          case "requester":
            requester = keyValue[1];
            break;
          case "provider":
            provider = keyValue[1];
            break;
          default:
            System.out.println("(parse-rfs-request): WARNING: Received unknown data : "
                               + Arrays.deepToString(request.toArray()));
        }
      }
      if(LogFunctionality.DEBUG) 
        System.out.println("(parse-rfs-request): RFS request parsed! "
                                                    + Arrays.deepToString(new Object[] { provider,
                                                                                        id,
                                                                                        time,
                                                                                        product,
                                                                                        requester }));
      return new String[] { provider,
                           id,
                           time,
                           product,
                           requester };
    }
  }
  
}
