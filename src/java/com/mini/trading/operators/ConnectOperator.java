package com.mini.trading.operators;

import java.io.IOException;
import java.net.Socket;

import com.ohua.lang.Function;
import com.mini.trading.operators.LogFunctionality;

public class ConnectOperator {
  
  @Function
  public Object[] connect(String addr, String port_param){
    
    Socket socket;
    Integer port = new Integer(port_param);
    try {
      socket = new Socket(addr, port);
      if(LogFunctionality.DEBUG)
        System.out.println("(connect): connected to " + addr + ":" + port);
      return new Object[] {socket};
    } catch (IOException e) {
      e.printStackTrace();
      System.out.println(e.getMessage());
      return null;
    }
    
  }
}
