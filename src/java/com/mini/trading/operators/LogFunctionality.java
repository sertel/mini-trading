package com.mini.trading.operators;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import com.ohua.lang.Function;
import com.ohua.lang.compile.analysis.qual.ReadOnly;

public abstract class LogFunctionality
{
  public static boolean DEBUG = false;
  
	public static class Logger
	{
		@Function
		public Object[] logEntry(String entry, List<String> log, boolean logIt) {
			long timeInMillis = GregorianCalendar.getInstance().getTimeInMillis();
			Timestamp time = new Timestamp(timeInMillis);
			if(logIt)
			  log.add(time + " : " + entry);
			if(DEBUG) System.out.println("(log-entry): " + entry);
			return new Object[] {};
		}
	}
	
	public static class PriceInfoLogEntryBuilder
	  {
	    @Function
	    public Object[] buildPriceLogEntry(String id, String time, String rfsID, String buyPrice, String sellPrice,
	                                       String provider)
	    {
	      return new Object[] { id + " : " + time + " : " + rfsID + " : " + buyPrice + " : " + sellPrice + " : "
	                            + provider };
	    }
	  }
	  
	  public static class RfsLogEntryBuilder
	  {
	    @Function
	    public Object[] buildRfsLogEntry(String provider, String id, String time, String product, String requester) {
	      return new Object[] { id + " : " + provider + " : " + time + " : " + product + " : " + requester };
	    }
	  }
	  
	  public static class OrderLogEntryBuilder
	  {
	    @Function
	    public Object[] buildOrderLogEntry(@ReadOnly Map<?, ?> order) {
	      StringBuffer buf = new StringBuffer();
	      for(Map.Entry<?, ?> e : order.entrySet()) {
	        buf.append(e.getKey());
	        buf.append(" : ");
	        buf.append(e.getValue());
	        buf.append("\n");
	      }
	      return new Object[] { buf.toString() };
	    }
	  }
	  
	public static class ReportLogEntryBuilder
	  {
	    @Function
	    public Object[] buildReportLogEntry(@ReadOnly List<String> report) {
	      return new Object[] { Arrays.deepToString(report.toArray()) };
	    }
	  }
	
}
