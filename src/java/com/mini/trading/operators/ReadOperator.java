package com.mini.trading.operators;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.lang.Function;
import com.mini.trading.operators.LogFunctionality;

public class ReadOperator {

	@Function
	public Object[] read(Socket socket) {
		Assertion.invariant(!socket.isClosed());
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					socket.getInputStream(), "UTF-8"));
			List<String> msg = new ArrayList<String>();
			String line = null;
			// an empty line marks the end of the request message
			while (!(line = in.readLine()).equals("")) {
				msg.add(line);
			}
			if(LogFunctionality.DEBUG)
			  System.out.println("(read): received " + Arrays.deepToString(msg.toArray()));
			return new Object[] {msg, socket};

		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
			throw new RuntimeException(e1);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
}
