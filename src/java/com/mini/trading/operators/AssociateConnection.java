package com.mini.trading.operators;

import java.io.IOException;
import java.net.Socket;
import java.util.List;

import com.ohua.lang.Function;
import com.ohua.lang.compile.analysis.qual.ReadOnly;

public abstract class AssociateConnection
{
  public static class AttachToRequest
  {
    @Function
    public Object[] attachConnection(@ReadOnly List<String> request, String addr, long port) {
      try {
        Socket socket = new Socket(addr, (int) port);
        if(LogFunctionality.DEBUG)
          System.out.println("(attach-connection): connected to " + addr + ":" + port);
        return new Object[] { request,
                             socket };
      }
      catch(IOException e) {
        throw new RuntimeException(e);
      }
    }
  }
}
