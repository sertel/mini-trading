package com.mini.trading.operators;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import com.ohua.lang.Function;
import com.mini.trading.operators.LogFunctionality;

/*
 * The accept operator implemented with the new API.
 */
public class AcceptOperator {
	
	/*
	 * Later on even exception handling will be taken care of by Ohua.
	 */
	@Function
		public Object[] accept(ServerSocket serverSocket) {
		
		try {
		  if(LogFunctionality.DEBUG) 
		    System.out.println("(accept): waiting for accept on socket " + serverSocket + " ...");
			Socket accept = serverSocket.accept();
			if(LogFunctionality.DEBUG)
			  System.out.println("(accept): received cnn.");
			return new Object[] {accept};
			
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
