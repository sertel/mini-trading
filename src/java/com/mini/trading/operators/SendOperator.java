package com.mini.trading.operators;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.lang.Function;
import com.ohua.lang.compile.analysis.qual.ReadOnly;
import com.mini.trading.operators.LogFunctionality;


public class SendOperator {
	
	@Function
	public Object[] send(@ReadOnly List<String> msg, Socket conn) {
		Assertion.invariant(!conn.isClosed());
		
		try {
			PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(conn.getOutputStream()));
			
			for (String msgLine : msg) {
				printWriter.println	(msgLine);
			}
			// end the request with a blank line
			printWriter.println("");
			printWriter.flush();

			if(LogFunctionality.DEBUG)
			  System.out.println("(send): sent " + Arrays.deepToString(msg.toArray()));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		return new Object[] {conn};
	}

}
