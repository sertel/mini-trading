package com.mini.trading.operators;

import com.ohua.lang.Function;

public class PipeOperator {

	@Function
	public Object[] pipe(Object o ) {
		return new Object[] {o};
	}
	
}
