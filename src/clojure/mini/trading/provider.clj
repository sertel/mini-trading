(ns mini.trading.provider
  (:use [com.ohua.compile]
        [com.ohua.transform]
        [com.ohua.logging]
        [mini.trading.core])
  (:require [mini.trading.core :all :refer :as conf])
  (:import java.net.InetAddress)
  )

(ohua :import [com.mini.trading.operators
               com.mini.trading.operators.provider
               com.mini.trading.operators.provider.benchmark])

(defn create-section-mapping []
  ; parallel
;  '( ["micro-input.*" "micro-read.*" "parse-request-type.*"]
;     ["parse-put-.*" "balance-10.*"] 
;     ["pipe-10.*" "merge-10.*" "build-put-msg-11.*" "send-11.*" "close-11.*"]
;     ["parse-rfs-request-.*" "balance-12.*"]
;     ["pipe-1[1|2].*" "merge-12.*" "build-price-msg-12.*" "send-12.*" "close-12.*"])
  ; sequential
;  '( ["micro-input.*" "micro-read.*" "parse-request-type.*"]
;     ["pipe-10.*" "build-put-msg-.*" "send-110" "close-109"]
;     ["pipe-1[1|2].*" "build-price-msg-.*" "send-118" "close-117"])
  ; Ohua vs STM
  ; STM config
;  '( ["micro-input.*" "micro-read.*" "parse-request-type.*"]
;     ["parse-put.*" "update-portfolio.*" "pipe-10.*" "build-put-msg-.*" "send-110" "close-109"]
;     ["parse-rfs-request-.*" "product-lookup.*" "pipe-1[1|2].*" "build-price-msg-.*" "send-118" "close-117"])
  ; Ohua config
  '( ["micro-input.*" "micro-read.*" "parse-request-type.*"]
     ["parse-put.*" "update-portfolio.*" "pipe-10.*" "build-put-msg-.*" "send-110" "close-109"
     "parse-rfs-request-.*" "product-lookup.*" "pipe-1[1|2].*" "build-price-msg-.*" "send-118" "close-117"])
  )

(defn configure []
  (let [props (new java.util.Properties)
        config (conf/parallel-exec-config props)]
      (doto props
        (.put "section-strategy" "com.mini.trading.test.LooseConfigurableSectionMapping")
        (.put "section-config" (create-section-mapping)))
      config))

(defn provide
  "Runs the provider."
  ([server products provider-id] (provide server products provider-id nil 1))
  ([server products provider-id micro-bench-input concurrent]
  ; Ohua debug and runtime configuration
  (set-debug-rt-config "provider")
  
;  (com.ohua.logging/enable-compilation-logging)
  
  ;  Ohua code starts here
  (ohua 
;    (let [in (micro-input micro-bench-input)
;          ]
;      (|| 8 concurrent 
          (let [
            ; receive requests
             [req read-conn] (read (accept server)) ;(micro-read in)
             [req-type content] (parse-request-type req)]
            (cond
              ; handle an internal provider request to update its repertoire of products
              (= "put" req-type) (let [
                                       old (update-portfolio (parse-put content) products)]
;                                   product (parse-put content)
;                                   old (|| 2 concurrent (update-portfolio product products) product)] 
                               (close (send (build-put-msg old) read-conn)))
        
              ; handle a rfs, returning a price
              (= "rfs" req-type) (let [ [_ rfs-id rfs-time product requester] (parse-rfs-request content)]
                           (close (send
                                    (build-price-msg 
                                      (product-lookup product products)
;                                      (|| 2 concurrent (product-lookup product products) product)
                                      rfs-id provider-id)
                                    read-conn)))
              ; handle an order, rejecting or processing and returning a report 
              (= "order" req-type) (let [ [order-id order-time rfs-id product is-requester-buying price-limit requester] (parse-provider-order-request content)]
                                     (let [[is-sold value] (process-order product price-limit is-requester-buying (product-lookup product products))] 
                                       (if (true? is-sold)
                                         (close (send (build-positive-report-msg order-id value requester provider-id) read-conn))
                                         (close (send (build-rejection-report-msg order-id requester provider-id) read-conn)))
                                       ))
        ))
;        in))
    
    ; Ohua runtime configuration
    (:run-with-config 
;      (conf/parallel-exec-config )
      (configure )
      ))))

(defn open-server
  "Open the server at the given port."
  [port]
  (println "Opening provider server at port" port "...")
  (let [server (doto (new java.net.ServerSocket port) (.setReuseAddress true))]
    (println "Server for the provider opened at port" port)
    server))

(defn close-server
  [server]
  (.close server)
  (println "Server closed!"))

(defn register
  "Registers as a provider at a trading node."
  [addr port, server, provider-id]
  (if (.isClosed server) (println "Registration failed, server needs to be opened first.") 
    (let
      [conn (new java.net.Socket addr port)
       out (new java.io.PrintWriter (new java.io.OutputStreamWriter (.getOutputStream conn)))]
      (.println out "type=register")
      (.println out (str "provider=" provider-id))
;      (println (str "addr=" (.getHostName (InetAddress/getLocalHost))))
      (.println out (str "addr=" (.getHostName (InetAddress/getLocalHost))))
      (.println out (str "port=" (.getLocalPort server)))
      (.println out "")
      (.flush out)
      (.close out)
      (.close conn))
    )
  )