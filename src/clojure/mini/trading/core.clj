(ns mini.trading.core)

(defmacro set-debug-rt-config
  "Sets configurations for debugging and the runtime execution."
  [name]
  (set! (. com.ohua.engine.utils.GraphVisualizer PRINT_FLOW_GRAPH) (str "test/" name "-flow"))
  (set! (. com.ohua.engine.utils.GraphVisualizer PRINT_SECTION_GRAPH) (str "test/" name "-flow-sections")))

(defn parallel-exec-config
  "Returns the ohua-configuration for parallel execution."
  ([] (parallel-exec-config (new java.util.Properties)))
  ([props]
  (doto (new com.ohua.engine.RuntimeProcessConfiguration)
    (.setProperties (doto props
                      (.setProperty "execution-mode" (.name (com.ohua.engine.RuntimeProcessConfiguration$Parallelism/MULTI_THREADED)))
                      (.setProperty "data-format" "com.ohua.engine.flowgraph.elements.operator.LanguageDataFormat")
                      (.setProperty "inter-section-arc-boundary" "100")
                      (.setProperty "inner-section-arc-boundary" "10")
                      (.setProperty "arc-activation" "1")
                      (.setProperty "core-thread-pool-size" "40")
                      (.setProperty "max-thread-pool-size" "40")
;                      (.setProperty "scheduler" "com.ohua.engine.scheduler.TaskScheduler")
                      ))))
  )