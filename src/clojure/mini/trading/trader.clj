(ns mini.trading.trader
  (:use [com.ohua.compile]
        [com.ohua.transform]
        [com.ohua.logging]
        [mini.trading.core])
  (:require [clojure.pprint])
  (:import [java.util HashMap ArrayList]
           [java.util.concurrent ConcurrentHashMap]
           ;[com.ohua.lang.runtime.stm STMHashMap]
           )
  )

(ohua :import [com.mini.trading.operators 
               com.mini.trading.operators.trader])

(def trading (atom true))
(def provs (new java.util.HashMap))
;(def provs (new java.util.concurrent.ConcurrentHashMap))
;(def provs (new com.ohua.lang.runtime.stm.STMHashMap))


; logs
(def log-it false)
(def requests (new java.util.ArrayList))
(def prices (new java.util.ArrayList))
(def orders (new java.util.ArrayList))
(def reports (new java.util.ArrayList))
(def configs (new java.util.ArrayList))

(defn trading-node
  "Runs the trading node."
  [server, concurrent]
  ; Ohua debug and runtime configuration
  (set-debug-rt-config "trading")

  ;  Ohua code starts here
;  (com.ohua.logging/enable-compilation-logging )

  (set! (. com.mini.trading.operators.LogFunctionality DEBUG) true)
  
  (ohua
    (let
      ; receive requests
      [[req conn-to-req] (read (accept server))
       [req-type content]  (parse-request-type req)]
      (cond
        ; en- /disable trading, requested by the admin
        (= "admin" req-type) (let [] 
                               (log-entry (build-admin-log-entry (set-trading (parse-admin-request content) trading)) 
                                 configs log-it)
                               (close conn-to-req))
        ; register a new provider
        (= "register" req-type) (let [prov (parse-register-request content)]
                                  (register prov provs)
;                              (|| 1 concurrent (register prov provs) prov)
                              (close conn-to-req))
        ; forward the received rfs to the registered provider and send the according offers to the requester
        (= "rfs" req-type) (let [[provider id time product requester] (parse-rfs-request content)]
                             (log-entry (build-rfs-log-entry id provider time product requester) requests log-it)
                             (let [provider-info (get-provider provider provs)]
;                               provider-info (|| 1 concurrent (get-provider provider provs) provider)]
                           ; TODO send negative report: FIX System Messages -> Reject
                           (if (= provider-info nil) (close conn-to-req) 
                             (let [[price-msg passed-conn] (read 
                                                             (send 
                                                               (build-rfs-msg id time product requester) 
                                                               (connect-to-prov provider-info)))
                               price (parse-price-info price-msg)]
                             (log-entry (build-price-log-entry price) prices log-it)
                             (close passed-conn)
                             (close (send price-msg conn-to-req))))))
        ; forward the received order to the provider and send the report to the requester
        (and (= "order" req-type)) ;(@trading))
                         (let [[provider order] (parse-order-request content)]
                           (log-entry (build-order-log-entry order) orders log-it)
                           (let [[addr port] 
;                                 (|| 2 concurrent 
                                     (get-provider provider provs) 
;                                     provider) 
                                 [report conn-to-prov] (read (send (build-order-msg order) (connect addr port)))]
                             (log-entry (build-report-log-entry report) reports log-it)
                             (close conn-to-prov)
                             (close (send report conn-to-req)))
                         )
;        (= 1 1) (report-unknown-request type)
        ))
    
      ; Ohua runtime configuration
      (:run-with-config
        (doto (new com.ohua.engine.RuntimeProcessConfiguration)
          (.setProperties (doto (new java.util.Properties)
                            (.setProperty "execution-mode" (.name (com.ohua.engine.RuntimeProcessConfiguration$Parallelism/MULTI_THREADED)))
                            (.setProperty "inter-section-arc-boundary" "100")
                            (.setProperty "inner-section-arc-boundary" "0")
                            (.setProperty "arc-activation" "1")
                            (.setProperty "core-thread-pool-size" "20")
                            (.setProperty "max-thread-pool-size" "20")
                            ; this quanta refers to the actions of a single op
                            (.setProperty "operator-quanta" "100")
                            ; this quanta refers to the scheduling operations of the operator scheduler
                            (.setProperty "scheduling-quanta" "200000")
;                            (.setProperty "inter-section-queue" "com.ohua.experiments.web.DeepCopyQueues$JavaSerializationDeepCopyQueue")
;                            (.setProperty "inter-section-queue" "com.ohua.experiments.web.DeepCopyQueues$KryoSerializationDeepCopyQueue")
                            ))))
      )
  )

(defn open-server
  "Open the server at the given port."
  [port]
  (let [server (doto (new java.net.ServerSocket port) (.setReuseAddress true))]
    (println "Server for the trading node opened at port" port)
    server))

(defn close-server
  [server]
  (.close server)
  (println "Server closed!"))

;(defn compiled []
;  (let*
;    [vec__639
;     (read (accept server))
;     req
;     (clojure.core/nth vec__639 0 nil)
;     conn-to-req
;     (clojure.core/nth vec__639 1 nil)
;     vec__640
;     (parse-request-type req)
;     type
;     (clojure.core/nth vec__640 0 nil)
;     content
;     (clojure.core/nth vec__640 1 nil)]
;    (if (= "admin" type)
;      (let*
;        []
;        (log-entry
;          (build-admin-log-entry
;            (set-trading (parse-admin-request content) trading))
;          configs
;          log-it)
;        (close conn-to-req))
;      (clojure.core/let [[content conn-to-req] (scope content conn-to-req)]
;        (if (= "register" type)
;          (clojure.core/let [[content conn-to-req] (scope content conn-to-req)]
;            (let*
;              [prov (parse-register-request content)]
;              (register prov provs)
;              (close conn-to-req)))
;          (clojure.core/let [[content conn-to-req] (scope content conn-to-req)]
;            (if (= "rfs" type)
;              (clojure.core/let [[content conn-to-req] (scope content conn-to-req)]
;                (let*
;                  [vec__641
;                   (parse-rfs-request content)
;                   provider
;                   (clojure.core/nth vec__641 0 nil)
;                   id
;                   (clojure.core/nth vec__641 1 nil)
;                   time
;                   (clojure.core/nth vec__641 2 nil)
;                   product
;                   (clojure.core/nth vec__641 3 nil)
;                   requester
;                   (clojure.core/nth vec__641 4 nil)]
;                  (log-entry
;                    (build-rfs-log-entry
;                      id
;                      provider
;                      time
;                      product
;                      requester)
;                    requests
;                    log-it)
;                  (let*
;                    [provider-info (get-provider provider provs)]
;                    (if (= provider-info nil)
;                      (clojure.core/let [[conn-to-req]
;                                         (scope conn-to-req)]
;                        (close conn-to-req))
;                      (clojure.core/let [[id product requester provider-info conn-to-req] (scope id product requester provider-info conn-to-req)]
;                        (let*
;                          [vec__642
;                           (read
;                             (send
;                               (build-rfs-msg id time product requester)
;                               (connect-to-prov provider-info)))
;                           price-msg
;                           (clojure.core/nth vec__642 0 nil)
;                           passed-conn
;                           (clojure.core/nth vec__642 1 nil)
;                           price
;                           (parse-price-info price-msg)]
;                          (log-entry
;                            (build-price-log-entry price)
;                            prices
;                            log-it)
;                          (close passed-conn)
;                          (close (send price-msg conn-to-req))))))))
;              (clojure.core/let [[content conn-to-req] (scope
;                                                         content
;                                                         conn-to-req)]
;                (if (= "order" type)
;                  (clojure.core/let [[content conn-to-req]
;                                     (scope content conn-to-req)]
;                    (let*
;                      [vec__643
;                       (parse-order-request content)
;                       provider
;                       (clojure.core/nth vec__643 0 nil)
;                       order
;                       (clojure.core/nth vec__643 1 nil)]
;                      (log-entry
;                        (build-order-log-entry order)
;                        orders
;                        log-it)
;                      (let*
;                        [vec__644
;                         (get-provider provider provs)
;                         addr
;                         (clojure.core/nth vec__644 0 nil)
;                         port
;                         (clojure.core/nth vec__644 1 nil)
;                         vec__645
;                         (read
;                           (send
;                             (build-order-msg order)
;                             (connect addr port)))
;                         report
;                         (clojure.core/nth vec__645 0 nil)
;                         conn-to-prov
;                         (clojure.core/nth vec__645 1 nil)]
;                        (log-entry
;                          (build-report-log-entry report)
;                          reports
;                          log-it)
;                        (close conn-to-prov)
;                        (close (send report conn-to-req)))))
;                  nil))))))))
;)