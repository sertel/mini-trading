(ns mini.trading.requester
  (:use [com.ohua.compile]
        [com.ohua.logging]
        [mini.trading.core]))

(ohua :import [com.mini.trading.operators
               com.mini.trading.operators.requester])

; logs
(def requests (new java.util.ArrayList))
(def prices (new java.util.ArrayList))
(def orders (new java.util.ArrayList))
(def reports (new java.util.ArrayList))

(defn request
  "Sends a request for a certain product, handles the according offer, orders and stores the report."
  [addr port]
  ; Ohua debug configuration
  (set-debug-rt-config "requester")
  
;  (com.ohua.logging/enable-compilation-logging)
  
  ; Ohua code starts here
  (ohua 
    ; issue an rfs request to the trader
    (let [rfs (create-rfs "requester-A" "someProduct")]
      (log-entry (build-report-log-entry rfs) requests)
      (let [[result conn] (read (send (attach-connection rfs addr port)))
            [order-decision order-info] (eval-price result)]
        (log-entry (build-report-log-entry result) prices)
        (close conn)
        ; if the decision is "buy" then issue an order request to the trader
        (if (.equalsIgnoreCase "buy" order-decision)
          (let [order (create-order order-info "requester-A")]
            (log-entry (build-report-log-entry order) orders)
            (let [[report passed-conn] (read (send (attach-connection order addr port)))]
              (log-entry (build-report-log-entry report) reports)
              (close passed-conn)
            ))))
      )
    
     ; Ohua runtime configuration
     (:run-with-config
      (doto (new com.ohua.engine.RuntimeProcessConfiguration)
        (.setProperties (doto (new java.util.Properties)
                          (.setProperty "execution-mode" (.name (com.ohua.engine.RuntimeProcessConfiguration$Parallelism/MULTI_THREADED)))
                          (.setProperty "inter-section-arc-boundary" "100")
                          (.setProperty "inner-section-arc-boundary" "0")
                          (.setProperty "arc-activation" "1")
                          ))))
    )
  )
