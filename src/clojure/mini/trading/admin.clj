(ns mini.trading.admin)

(defn send-msg
  "Registers at a trading node as a provider."
  [addr port msg]
  (let
    [conn (new java.net.Socket addr port)
     out (new java.io.PrintWriter (new java.io.OutputStreamWriter (.getOutputStream conn)))]
    (.println out "type=admin")
    (.println out msg)
    (.println out "")
    (.flush out)
    (.close out)
    (.close conn)
    )
  )

(defn enable-trading
  [addr port]
  (send-msg addr port "trading=enabled")
  )

(defn disable-trading
  [addr port]
  (send-msg addr port "trading=disabled")
  )