/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.mini.trading.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import soot.ArrayType;
import soot.PackManager;
import soot.PatchingChain;
import soot.PointsToAnalysis;
import soot.PointsToSet;
import soot.RefType;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Type;
import soot.Unit;
import soot.ValueBox;
import soot.jimple.AssignStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.Stmt;
import soot.options.Options;
import soot.util.Chain;

public class SootReferenceAnalysis {
  
  public void initialize(boolean enableGEOPTA) {
    // FIXME this seems to be os dependent and therefore really needs to be more elaborate.
    // instead of checking the os version it would be better to locate these files via the new
    // nio mechanisms.
    StringBuffer sb = new StringBuffer();
    sb.append(System.getProperty("java.class.path") + File.pathSeparator);
    sb.append(System.getProperty("java.home"));
    sb.append(File.separator);
    sb.append("lib");
    sb.append(File.separator);
    sb.append("rt.jar");
    sb.append(File.pathSeparator + System.getProperty("java.home") + File.separator + "lib" + File.separator
              + "jce.jar");
    
    // configure soot (it tries to do something sophisticated in Scene.getDefaultClasspath for
    // MAC OS X and fails)
    Options.v().set_soot_classpath(sb.toString());
    Options.v().set_whole_program(true); // apparently needed
    Options.v().setPhaseOption("cg.spark", "on");
    Options.v().setPhaseOption("cg.spark", "enabled:true");
//    Options.v().setPhaseOption("cg.paddle", "on"); // outdated and not maintained.
    if(enableGEOPTA) Options.v().setPhaseOption("cg.spark", "geom-pta:true");
    else Options.v().setPhaseOption("cg.spark", "geom-pta:false");
    Options.v().set_output_format(Options.output_format_jimple); // produce jimple
  }
  
  public Set<String> analyze(Method m) throws FileNotFoundException, IOException {
    SootMethod goMethod = chargeSoot(m);
    // List<ValueBox> containedRefs = collectElements(goMethod);
    
    return Collections.emptySet();
  }
  
  private List<PointsToSet> collect(PointsToAnalysis analysis, PointsToSet pts, Type l) {
    List<PointsToSet> result = new ArrayList<>();
    Type t = l instanceof ArrayType ? ((ArrayType) l).getElementType() : l;
    if(t instanceof RefType) {
      SootClass s = ((RefType) t).getSootClass();
      Chain<SootField> fields = s.getFields();
      for(SootField f : fields) {
        PointsToSet newPts = analysis.reachingObjects(pts, f);
        result.add(newPts);
        result.addAll(collect(analysis, newPts, f.getType()));
      }
    } else {
      // no need to compare that thing as it is a primitive type and hence immutable
    }
    return result;
  }
  
  private List<ValueBox> collectElements(SootMethod goMethod) {
    ValueBox resultArray = ((ReturnStmt) goMethod.getActiveBody().getUnits().getLast()).getOpBox();
    // walk through the body and check where the elements come from
    PatchingChain<Unit> body = goMethod.getActiveBody().getUnits();
    List<ValueBox> containedRefs = new ArrayList<>();
    for(Unit unit : body) {
      Stmt stmt = (Stmt) unit; // necessary down cast
      if(stmt.containsArrayRef() && stmt.getArrayRef().getBaseBox().getValue().equals(resultArray.getValue())
         && stmt instanceof AssignStmt)
      {
        containedRefs.add(((AssignStmt) stmt).getRightOpBox());
      }
    }
    return containedRefs;
  }
  
  private SootMethod chargeSoot(Method m) {
    // load class to be inspected into Soot
    SootClass c = Scene.v().forceResolve(m.getDeclaringClass().getName(), SootClass.BODIES);
    c.setApplicationClass();
    Scene.v().loadNecessaryClasses();
    SootMethod method = findMethod(c, m);
    List<SootMethod> entryPoints = new ArrayList<>();
    entryPoints.add(method);
    Scene.v().setEntryPoints(entryPoints);
    PackManager.v().runPacks();
    return method;
  }
  
  private SootMethod findMethod(SootClass c, Method m) {
    // convert the parameters to Soot types
    // Java 1.7
    List<soot.Type> params = new ArrayList<>(m.getParameterTypes().length);
    // Java 1.8
    // List<soot.Type> params = new ArrayList<>(m.getParameterCount());
    for(Class<?> jt : m.getParameterTypes())
      params.add(Scene.v().getSootClass(jt.getName()).getType());
    try {
      return c.getMethod(m.getName(), params);
    }
    catch(Throwable t) {
      assert false;
    }
    return null;
  }
}
