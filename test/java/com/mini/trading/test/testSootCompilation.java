package com.mini.trading.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Map;

import org.junit.Test;

import com.mini.trading.test.testOhuaCompilation.ArrayListAdd;
import com.mini.trading.test.testOhuaCompilation.HashMapPut;

public class testSootCompilation {
  
  @Test
  public void compileProvider() throws Throwable {
    provider(false);
  }
  
  public static void provider(boolean geo) throws FileNotFoundException, IOException, NoSuchMethodException {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize(geo);
    long start = System.currentTimeMillis();
    analysis.analyze(ProviderJavaAlgo.class.getMethod("run", ServerSocket.class, Map.class, String.class));
    long time = (System.currentTimeMillis() - start);
    System.gc();
    System.gc();
    System.gc();
    Runtime rt = Runtime.getRuntime();
    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
    System.out.println("{ \"lib\" : \"soot\", \"time(s)\" : " + time + ", \"space(MB)\" : " + usedMB + ", \"type\" : \"trading\", \"geo-enabled\" : " + geo + " }");
  }
  
  @Test
  public void compileRequester() throws Throwable {
    requester(false);
  }
  
  public static void requester(boolean geo) throws FileNotFoundException, IOException, NoSuchMethodException {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize(geo);
    long start = System.currentTimeMillis();
    analysis.analyze(RequesterJavaAlgo.class.getMethod("run", String.class, Long.class));
    long time = (System.currentTimeMillis() - start);
    System.gc();
    System.gc();
    System.gc();
    Runtime rt = Runtime.getRuntime();
    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
    System.out.println("{ \"lib\" : \"soot\", \"time(s)\" : " + time + ", \"space(MB)\" : " + usedMB + ", \"type\" : \"trading\", \"geo-enabled\" : " + geo + " }");
  }
  
  @Test
  public void compileTrading() throws Throwable {
    trading(false);
  }
  
  public static void trading(boolean geo) throws FileNotFoundException, IOException, NoSuchMethodException {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize(geo);
    long start = System.currentTimeMillis();
    analysis.analyze(TradingPlatformJavaAlgo.class.getMethod("run", ServerSocket.class, Map.class));
    long time = (System.currentTimeMillis() - start);
    System.gc();
    System.gc();
    System.gc();
    Runtime rt = Runtime.getRuntime();
    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
    System.out.println("{ \"lib\" : \"soot\", \"time(ms)\" : " + time + ", \"space(MB)\" : " + usedMB + ", \"type\" : \"trading\", \"geo-enabled\" : " + geo + " }");
  }
  
  @Test
  public void compileArrayList() throws Throwable{
    arrayList(true);
  }
  
  public static void arrayList(boolean geo) throws Throwable{
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize(geo);
    long start = System.currentTimeMillis();
    analysis.analyze(ArrayListAdd.class.getMethod("go"));
//    analysis.analyze(ArrayList.class.getMethod("add", Object.class));
    long time = (System.currentTimeMillis() - start);
    System.gc();
    System.gc();
    System.gc();
    Runtime rt = Runtime.getRuntime();
    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
    System.out.println("{ \"lib\" : \"soot\", \"time(ms)\" : " + time + ", \"space(MB)\" : " + usedMB + ", \"type\" : \"trading\", \"geo-enabled\" : " + geo + " }");
  }

  @Test
  public void compileHashMap() throws Throwable{
    hashMap(false);
  }
  
  public static void hashMap(boolean geo) throws Throwable{
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize(geo);
    long start = System.currentTimeMillis();
//    analysis.analyze(HashMap.class.getMethod("put", Object.class, Object.class));
    analysis.analyze(HashMapPut.class.getMethod("go"));
    long time = (System.currentTimeMillis() - start);
    System.gc();
    System.gc();
    System.gc();
    Runtime rt = Runtime.getRuntime();
    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
    System.out.println("{ \"lib\" : \"soot\", \"time(ms)\" : " + time + ", \"space(MB)\" : " + usedMB + ", \"type\" : \"trading\", \"geo-enabled\" : " + geo + " }");
  }

  /**
   * Perform experiments from this hook. Execute via:<br>
   * lein run -m com.mini.trading.test.testSootCompilation
   * 
   * @param args
   * @throws Throwable
   */
  public static void main(String[] args) throws Throwable {
    System.out.println("------------------------------------------------------------");
    System.out.println("Running compilation for '" + args[0] + "' :");
    switch(args[0]) {
      case "p":
        provider(false);
        break;
      case "p-geo":
        provider(true);
        break;
      case "r":
        requester(false);
        break;
      case "r-geo":
        requester(true);
        break;
      case "t":
        trading(false);
        break;
      case "t-geo":
        trading(true);
        break;
      case "al":
        arrayList(false);
        break;
      case "al-geo":
        arrayList(true);
        break;
      case "hm":
        hashMap(false);
        break;
      case "hm-geo":
        hashMap(true);
        break;
      default:
        System.out.println("Unknown option: " + args[0]);
    }
    System.out.println("------------------------------------------------------------");
  }
}
