package com.mini.trading.test;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.mini.trading.operators.AssociateConnection.AttachToRequest;
import com.mini.trading.operators.CloseOperator;
import com.mini.trading.operators.LogFunctionality.Logger;
import com.mini.trading.operators.LogFunctionality.ReportLogEntryBuilder;
import com.mini.trading.operators.ReadOperator;
import com.mini.trading.operators.SendOperator;
import com.mini.trading.operators.requester.CreateOrderOperator;
import com.mini.trading.operators.requester.CreateRfsOperator;
import com.mini.trading.operators.requester.EvaluatePriceOperator;

/**
 * Created just for experimental purposes.
 * 
 * @author sertel
 *
 */
public class RequesterJavaAlgo {
  /**
   * ; issue an rfs request to the trader<br>
   * (let [rfs (create-rfs "requester-A" "someProduct")]<br>
   * (log-entry (build-report-log-entry rfs) requests)<br>
   * (let [[result conn] (read (send (attach-connection rfs addr port)))<br>
   * [order-decision order-info] (eval-price result)]<br>
   * (log-entry (build-report-log-entry result) prices)<br>
   * (close conn)<br>
   * ; if the decision is "buy" then issue an order request to the trader<br>
   * (if (.equalsIgnoreCase "buy" order-decision)<br>
   * (let [order (create-order order-info "requester-A")]<br>
   * (log-entry (build-report-log-entry order) orders)<br>
   * (let [[report passed-conn] (read (send (attach-connection order addr port)))]<br>
   * (log-entry (build-report-log-entry report) reports)<br>
   * (close passed-conn)<br>
   * ))))<br>
   */
  @SuppressWarnings("unchecked")
  public void run(String addr, Long port) {
    ArrayList<String> requests = new ArrayList<>();
    ArrayList<String> prices = new ArrayList<>();
    ArrayList<String> orders = new ArrayList<>();
    ArrayList<String> reports = new ArrayList<>();
    boolean logIt = true;
    
    Object[] rfs = new CreateRfsOperator().createRfs("requester-A", "someProduct");
    new Logger().logEntry((String) new ReportLogEntryBuilder().buildReportLogEntry((List<String>) rfs[0])[0],
                          requests,
                          logIt);
    Object[] msgAndCnn = new AttachToRequest().attachConnection((List<String>) rfs[0], addr, port);
    Object[] rfsResp =
        new ReadOperator().read((Socket) new SendOperator().send((List<String>) msgAndCnn[0],
                                                                 (Socket) msgAndCnn[1])[0]);
    new Logger().logEntry((String) new ReportLogEntryBuilder().buildReportLogEntry((List<String>) rfsResp[0])[0],
                          prices,
                          logIt);
    new CloseOperator().close((Socket) rfsResp[1]);
    
    Object[] evaluated = new EvaluatePriceOperator().evalPrice((List<String>) rfsResp[0]);
    if("buy".equalsIgnoreCase((String) evaluated[0])) {
      Object[] order = new CreateOrderOperator().createOrder((List<String>) evaluated[1], "requester-A");
      new Logger().logEntry((String) new ReportLogEntryBuilder().buildReportLogEntry((List<String>) order[0])[0],
                            orders,
                            logIt);
      msgAndCnn = new AttachToRequest().attachConnection((List<String>) order[0], addr, port);
      Object[] orderResp =
          new ReadOperator().read((Socket) new SendOperator().send((List<String>) msgAndCnn[0],
                                                                   (Socket) msgAndCnn[1])[0]);
      new Logger().logEntry((String) new ReportLogEntryBuilder().buildReportLogEntry((List<String>) orderResp[0])[0],
                            reports,
                            logIt);
      new CloseOperator().close((Socket) orderResp[1]);
    }
  }
}
