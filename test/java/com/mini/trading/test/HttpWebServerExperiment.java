/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.mini.trading.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.apache.commons.math3.distribution.UniformIntegerDistribution;

public class HttpWebServerExperiment {
  
  public enum RequestType {
    GET,
    PUT
  }
  
  public static abstract class Request {
    long start = 0;
    long stop = 0;
    
    long c_start = 0;
    long c_stop = 0;
    
    boolean failed = false;
    
    private void
        perform(BufferedReader reader, PrintWriter writer, String resource, String address) throws IOException
    {
      // start = System.nanoTime();
      start = System.currentTimeMillis();
      String expectedFirstLine = writeRequest(writer, resource, address);
      writer.flush();
      // System.out.println("HTTP request sent. " + resource);
      
      // wait for the response
      
      // @SuppressWarnings("unused")
      String firstLine = null;
      String line = null;
      while((line = reader.readLine()) != null) {
        if(firstLine == null) {
          firstLine = line;
        }
        // System.out.println("received line:" + line);
      }
      if(firstLine != null) {
        firstLine.trim();
        assert firstLine.endsWith(expectedFirstLine);
      } else {
        if(expectedFirstLine != null) System.out.println("Received empty response.");
      }
      // System.out.println("Response received.");
      // stop = System.nanoTime();
      stop = System.currentTimeMillis();
    }
    
    protected abstract String writeRequest(PrintWriter writer, String resource, String address);
  }
  
  public static class GetRequest extends Request {
    protected String writeRequest(PrintWriter writer, String resource, String address) {
      writer.println("GET " + resource + " HTTP/1.1");
      writer.println("Host: " + address);
      writer.println("From: sertel@ohua.com");
      writer.println("Cache-Control: no-store");
      writer.println("User-Agent: HTTPTool/1.0");
      writer.println("Connection: close");
      writer.println("");
      return "200 OK";
    }
  }
  
  public static class PutRequest extends Request {
    
    private int _size = 0;
    
    public PutRequest(int size) {
      _size = size;
    }
    
    protected String writeRequest(PrintWriter writer, String resource, String address) {
      writer.println("PUT " + resource + " HTTP/1.1");
      writer.println("Host: " + address);
      writer.println("From: sertel@ohua.com");
      writer.println("Cache-Control: no-store");
      writer.println("User-Agent: HTTPTool/1.0");
      writer.println("Connection: close");
      writer.println("Content: " + generateContents(_size));
      writer.println("");
      return "200 OK";
    }
  }
  
  public static final String generateContents(int size) {
    final String TEST_DATA = "test-data:";
    StringBuffer content = new StringBuffer();
    while(content.length() < size) {
      content.append(TEST_DATA);
    }
    if(content.length() > size) {
      content.delete(size, size + content.length());
    }
    return content.toString();
  }
  
  public static class Client implements Runnable {
    private List<Request> _requests = new ArrayList<Request>();
    private String _serverAddress = null;
    private int _port = 0;
    private int _currentPort = 0;
    private String _url = null;
    private int _serverCount = 1;
    
    private RequestType _type = null;
    private int _contentSize = 0;
    
    private UniformIntegerDistribution _distribution = null;
    
    private Socket _socket = null;
    private BufferedReader _reader = null;
    private PrintWriter _writer = null;
    private int _requestCount = 0;
    
    private int _failed = 0;
    private int _succeeded = 0;
    private int _crashed = 0;
    
    public Client(String serverAddress,
                  int port,
                  int startingPort,
                  int serverCount,
                  int upperFileBound,
                  String url,
                  RequestType requestType,
                  int contentSize)
    {
      _serverAddress = serverAddress;
      _port = port;
      _serverCount = serverCount;
      _currentPort = startingPort;
      if(upperFileBound > 0)
        _distribution = new UniformIntegerDistribution(0, upperFileBound);
      _url = url;
      _type = requestType;
      _contentSize = contentSize;
    }
    
    @Override
    public void run() {
      Request r = createRequest();
      try {
        requestResource(r);
      }
      catch(Exception e) {
        e.printStackTrace();
        throw new RuntimeException();
      }
      _requests.add(r);
    }
    
    protected Request createRequest() {
      switch(_type) {
        case GET:
          return new GetRequest();
        case PUT:
          return new PutRequest(_contentSize);
        default:
          throw new RuntimeException("impossible");
      }
    }
    
    private void requestResource(Request request) throws IOException {
      // FIXME This actually requires a stateful server (with respect to the accepted
      // connections)!
      if(_requestCount == 0) {
        if(_socket != null) {
          // System.out.println("Socket refreshed");
          _reader.close();
          _writer.close();
          _socket.close();
          _socket = null;
        }
        
        while(_socket == null) {
          // System.out.println("Opening connection to " + _serverAddress + ":" + _port);
          try {
            // request.c_start = System.nanoTime();
            request.c_start = System.currentTimeMillis();
            _socket = new Socket(_serverAddress, _currentPort);
            // _socket = new Socket();
            // _socket.connect(new java.net.InetSocketAddress(_serverAddress, _port), 2000);
            // request.c_stop = System.nanoTime();
            request.c_stop = System.currentTimeMillis();
          }
          catch(ConnectException | SocketTimeoutException e) {
            request.failed = true;
            _failed++;
            _socket = null;
            System.out.println("Connection failed on port:" + _currentPort);
            // System.out.println("Connection failed at " + System.currentTimeMillis());
            // System.out.println("Connection failed at " + System.nanoTime());
            // try
            // {
            // Thread.sleep(5);
            // }
            // catch(InterruptedException e1)
            // {
            // e1.printStackTrace();
            // }
            continue;
          }
          _succeeded++;
          _currentPort = (((_currentPort - _port) + 1) % _serverCount) + _port;
          // System.out.println(_socket.isConnected());
          _reader = new BufferedReader(new InputStreamReader(_socket.getInputStream()));
          _writer = new PrintWriter(new OutputStreamWriter(_socket.getOutputStream(), "UTF-8"));
        }
      }
      
      try {
        request.perform(_reader, _writer, findResource(), _serverAddress + ":" + _port);
      }
      catch(IOException ioe) {
        _crashed++;
      }
      
      _requestCount = (_requestCount + 1) % 1;
      // System.out.println("Request count: " + _requestCount);
    }
    
    private String findResource() {
      return _url + "/test_" + getDistributionSample();
    }
    
    public int getDistributionSample() {
      if(_distribution == null) return 0;
      else return _distribution.sample();
    }
    
    // /**
    // * We print JSON!
    // * @return
    // */
    // public void appendStatistics(StringBuffer contents)
    // {
    // for(Request r : _requests)
    // {
    // contents.append("{\"start\":");
    // contents.append(r.start);
    // contents.append(", ");
    // contents.append("\"stop\":");
    // contents.append(r.stop);
    // contents.append("},\n");
    // }
    // }
    
    public double[][] collapseStatistics(long start, StringBuffer contents) {
      // data transfer
      double avg = 0;
      double max = 0;
      // connection setup
      double c_avg = 0;
      double c_max = 0;
      int i = 0;
      while(i < _requests.size()) {
        // long stop = start + 1000000000;
        long stop = start + 100;
        ArrayList<Request> requests = new ArrayList<Request>();
        while(_requests.get(i).start < stop) {
          requests.add(_requests.get(i++));
          if(i >= _requests.size()) {
            // don't bother the last second
            break;
          }
        }
        if(!requests.isEmpty()) {
          double[] stats = collapsePerSecond(requests);
          double avg_latency = stats[0];
          double avg_conn_time = stats[1];
          contents.append("{\"start\":");
          contents.append(start);
          contents.append(", ");
          contents.append("\"latency\":");
          contents.append(avg_latency);
          contents.append(", ");
          contents.append("\"count\":");
          contents.append(requests.size());
          contents.append(", ");
          contents.append("\"connect (wait) time\":");
          contents.append(avg_conn_time);
          contents.append("},\n");
          if(avg == 0) {
            avg = avg_latency;
            c_avg = avg_conn_time;
          } else {
            avg += avg_latency;
            avg = avg / 2;
            
            c_avg += avg_conn_time;
            c_avg = c_avg / 2;
          }
          max = Math.max(max, avg_latency);
          c_max = Math.max(c_max, avg_conn_time);
        }
        start = stop;
      }
      return new double[][] { new double[] { avg,
                                            max },
                             new double[] { c_avg,
                                           c_max } };
    }
    
    private double[] collapsePerSecond(ArrayList<Request> requests) {
      assert requests.size() > 0;
      long sum = 0;
      long c_sum = 0;
      for(Request r : requests) {
        if(!r.failed) {
          sum += r.stop - r.start;
          c_sum += r.c_stop - r.c_start;
        }
      }
      return new double[] { ((double) sum) / requests.size(),
                           ((double) c_sum) / requests.size() };
    }
  }
  
  /**
   * Run the simple experiment here by starting a certain number of clients (=threads).</p>
   * Configuration parameters are:
   * <ul>
   * <li>client-count = number of threads in the thread pool
   * <li>request-rate = rate in which requests are being submitted by the clients
   * </ul>
   * @author sertel
   * 
   */
  public static class Experiment {
    private int _clientCount = 0;
    private int _requestDelay = 0;
    private long _experimentLength = 0;
    
    private String _serverAddress = null;
    private int _port = 0;
    private int _serverCount = 0;
    private int _upperFileBound = 0;
    private String _url = null;
    
    private RequestType _type = null;
    private int _contentSize = 0;
    
    private ScheduledThreadPoolExecutor _executor = null;
    
    private List<Client> _clients = new ArrayList<Client>();
    
    public void execute(Properties configuration) throws FileNotFoundException, IOException, InterruptedException {
      applyConfiguraiton(configuration);
      prepare();
      run();
    }
    
    protected void execute(String configuration) throws FileNotFoundException, IOException, InterruptedException {
      loadConfiguration(configuration);
      prepare();
      run();
    }
    
    protected void run() throws InterruptedException {
      System.out.println("Target: " + _serverAddress + ":" + _port);
      int startingPort = _port + (new UniformIntegerDistribution(0, _serverCount).sample() % _serverCount);
      for(int i = 0; i < _clientCount; i++) {
        Client c =
            createClient(_serverAddress,
                         _port,
                         startingPort,
                         _serverCount,
                         _upperFileBound,
                         _url,
                         _type,
                         _contentSize);
        _executor.scheduleWithFixedDelay(c, 0, _requestDelay, TimeUnit.MILLISECONDS);
        _clients.add(c);
        startingPort = _port + ((startingPort + 1) % _serverCount);
      }
      System.out.println("Waiting for experiment to finish ...");
      Thread.sleep(_experimentLength);
      _executor.shutdown();
      // _executor.awaitTermination(5000, TimeUnit.MILLISECONDS);
    }
    
    protected Client createClient(String serverAddress, int port, int startingPort, int serverCount,
                                  int upperFileBound, String url, RequestType requestType, int contentSize)
    {
      return new Client(serverAddress,
                        port,
                        startingPort,
                        serverCount,
                        upperFileBound,
                        url,
                        requestType,
                        contentSize);
    }
    
    protected void prepare() {
      _executor = new ScheduledThreadPoolExecutor(_clientCount);
      _executor.setMaximumPoolSize(_clientCount);
      _executor.setContinueExistingPeriodicTasksAfterShutdownPolicy(false);
      _executor.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
      // make all threads daemons such that they are being killed forcefully if they have not
      // completed after the timeout. otherwise the JVM won't exit.
      _executor.setThreadFactory(new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
          Thread thread = new Thread(r);
          thread.setDaemon(true);
          return thread;
        }
      });
      _executor.prestartAllCoreThreads();
    }
    
    protected void loadConfiguration(String configuration) throws FileNotFoundException, IOException {
      Properties props = loadConfig(configuration);
      applyConfiguraiton(props);
    }
    
    protected static Properties loadConfig(String configuration) throws FileNotFoundException, IOException {
      Properties props = new Properties();
      props.load(new FileReader(configuration));
      return props;
    }
    
    protected void applyConfiguraiton(Properties props) {
      _clientCount = Integer.parseInt(props.getProperty("client-count"));
      _requestDelay = Integer.parseInt(props.getProperty("request-delay"));
      _experimentLength = Integer.parseInt(props.getProperty("experiment-length"));
      _serverAddress = props.getProperty("server-address");
      _port = Integer.parseInt(props.getProperty("server-port"));
      _serverCount = Integer.parseInt(props.getProperty("server-count", "1"));
      _upperFileBound = Integer.parseInt(props.getProperty("max-file-num"));
      _url = props.getProperty("url");
      _type = RequestType.valueOf(props.getProperty("request-type", "GET"));
      _contentSize = Integer.parseInt(props.getProperty("content-size", "512")); // for PUT
                                                                                 // requests
    }
    
    public void writeStatistics(String file, long start) throws IOException {
      writeFile(new File(file), generateStatistics(start));
    }
    
    public void writeFile(File file, String contents) throws IOException {
      FileWriter writer = new FileWriter(file);
      writer.write(contents);
      writer.flush();
      writer.close();
    }
    
    public String generateStatistics(long start) throws IOException {
      StringBuffer contents = new StringBuffer();
      contents.append("[");
      int failed = 0;
      int succeeded = 0;
      int crashed = 0;
      // data transfer
      double avg_latency = 0;
      double max = 0;
      // connection
      double c_avg_latency = 0;
      double c_max = 0;
      for(Client client : _clients) {
        failed += client._failed;
        succeeded += client._succeeded;
        crashed += client._crashed;
        
        double[][] stats = client.collapseStatistics(start, contents);
        double[] data_stats = stats[0];
        double[] cnn_stats = stats[1];
        
        avg_latency += data_stats[0];
        max = Math.max(max, data_stats[1]);
        
        c_avg_latency += cnn_stats[0];
        c_max = Math.max(c_max, cnn_stats[1]);
      }
      System.out.println("$ Requests >> Crashed: " + crashed + " Failed: " + failed + " Success: " + succeeded
                         + " Total: " + (failed + succeeded));
      System.out.println("# Data Statistics >> Avg: " + (avg_latency / _clients.size()) + " Max: " + max);
      System.out.println("# Connection Statistics >> Avg: " + (c_avg_latency / _clients.size()) + " Max: " + c_max);
      
      // for(Client client : _clients)
      // {
      // client.appendStatistics(contents);
      // }
      contents.delete(contents.length() - 2, contents.length());
      contents.append("]");
      return contents.toString();
    }
  }
}
