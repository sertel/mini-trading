package com.mini.trading.test;

import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.mini.trading.operators.AcceptOperator;
import com.mini.trading.operators.CloseOperator;
import com.mini.trading.operators.PipeOperator;
import com.mini.trading.operators.ReadOperator;
import com.mini.trading.operators.RequestHandling.RFSRequestParser;
import com.mini.trading.operators.RequestHandling.RequestTypeParser;
import com.mini.trading.operators.SendOperator;
import com.mini.trading.operators.provider.MessageBuilder.PositiveReportMessageBuilder;
import com.mini.trading.operators.provider.MessageBuilder.PriceMessageBuilder;
import com.mini.trading.operators.provider.MessageBuilder.RejectionReportMessageBuilder;
import com.mini.trading.operators.provider.PortfolioHandling.PutMsg;
import com.mini.trading.operators.provider.PortfolioHandling.PutParser;
import com.mini.trading.operators.provider.PortfolioHandling.Updater;
import com.mini.trading.operators.provider.ProviderFunctionality.OrderHandler;
import com.mini.trading.operators.provider.ProviderFunctionality.PriceCreator;
import com.mini.trading.operators.provider.ProviderFunctionality.ProductDetails;
import com.mini.trading.operators.provider.ProviderRequestHandling.OrderRequestParser;

/**
 * Created just for experimental purposes.
 * 
 * @author sertel
 *
 */
public class ProviderJavaAlgo {
  /**
   * [req read-conn] (read (accept server)<br>
   * [type content] (parse-request-type req)]<br>
   * (cond<br>
   * ; handle an internal provider request to update its repertoire of products<br>
   * (= "put" type) (let [new-read-conn (pipe read-conn)<br>
   * old (update-portfolio (parse-put content) products)]<br>
   * (close (send (build-put-msg old) new-read-conn)))<br>
   * <br>
   * ; handle a rfs, returning a price<br>
   * (= "rfs" type) (let [ [_ rfs-id rfs-time product requester] (parse-rfs-request content)<br>
   * new-read-conn (pipe read-conn)]<br>
   * (close (send<br>
   * (build-price-msg<br>
   * (product-lookup product products)<br>
   * rfs-id provider-id)<br>
   * new-read-conn)))<br>
   */
  @SuppressWarnings("unchecked")
  public void run(ServerSocket socket, Map<String, ProductDetails> products, String providerID) {
    Object[] readOutput = new ReadOperator().read((Socket) new AcceptOperator().accept(socket)[0]);
    Object[] parseOutput = new RequestTypeParser().parseRequestType((List<String>) readOutput[0]);
    switch((String) parseOutput[0]) {
      case "put":
        Object[] parsedPut = new PutParser().parsePut((List<String>) parseOutput[1]);
        Object[] old =
            new Updater().updatePortfolio((String) parsedPut[0], (ProductDetails) parsedPut[1], products);
        new CloseOperator().close((Socket) new SendOperator().send((List<String>) new PutMsg().buildPutMsg((ProductDetails) old[0])[0],
                                                                   (Socket) new PipeOperator().pipe(readOutput[1])[0])[0]);
        break;
      case "rfs":
        Object[] parsedRFS = new RFSRequestParser().parseRfsRequest((List<String>) parseOutput[1]);
        Object[] lookedUp = new PriceCreator().productLookup((String) parsedRFS[3], products);
        new CloseOperator().close((Socket) new SendOperator().send((List<String>) new PriceMessageBuilder().buildPriceMsg((Timestamp) lookedUp[0],
                                                                                                                          (ProductDetails) lookedUp[1],
                                                                                                                          (String) parsedRFS[1],
                                                                                                                          providerID)[0],
                                                                   (Socket) new PipeOperator().pipe(readOutput[1])[0])[0]);
        break;
      case "order":
        /**
         * (= "order" type) (let [ [order-id order-time rfs-id product is-requester-buying
         * price-limit requester] (parse-provider-order-request content)]<br>
         * (let [[is-sold value] (process-order product price-limit is-requester-buying
         * (product-lookup product products))]<br>
         * (if (true? is-sold)<br>
         * (close (send (build-positive-report-msg order-id value requester provider-id)
         * read-conn))<br>
         * (close (send (build-rejection-report-msg order-id requester value provider-id)
         * read-conn)))<br>
         * ))<br>
         */
        Object[] parsedOrder = new OrderRequestParser().parseProviderOrderRequest((List<String>) parseOutput[1]);
        Object[] processedOrder =
            new OrderHandler().processOrder((String) parsedOrder[3],
                                            (double) parsedOrder[5],
                                            (boolean) parsedOrder[4],
                                            (ProductDetails) new PriceCreator().productLookup((String) parsedOrder[3],
                                                                                              products)[0]);
        if((boolean) processedOrder[0]) {
          new CloseOperator().close((Socket) new SendOperator().send((List<String>) new PositiveReportMessageBuilder().buildPositiveReportMsg((String) parsedOrder[0],
                                                                                                                                              (double) processedOrder[1],
                                                                                                                                              (String) parsedOrder[6],
                                                                                                                                              providerID)[0],
                                                                     (Socket) readOutput[1])[0]);
        } else {
          new CloseOperator().close((Socket) new SendOperator().send((List<String>) new RejectionReportMessageBuilder().buildRejectionReportMsg((String) parsedOrder[0],
                                                                                                                                              (String) parsedOrder[6],
                                                                                                                                              providerID)[0],
                                                                     (Socket) readOutput[1])[0]);          
        }
        break;
      default:
        // not possible per App definition
        return;
    }
  }
}
