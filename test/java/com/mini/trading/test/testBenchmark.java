package com.mini.trading.test;

import org.junit.Test;

import clojure.java.api.Clojure;
import clojure.lang.IFn;

public class testBenchmark {
  @Test
  public void simpleTest() throws Throwable {
    Clojure.var("clojure.core", "require").invoke(Clojure.read("mini.trading.provider-test"));
    IFn mb = Clojure.var("mini.trading.provider-test", "micro-benchmark");
    // warm up
    Object result = mb.invoke("stm", "0.5", "100", "50", "2");
//    System.out.println("Time: " + result + "ms");
    
//    for(int i = 0; i < 11; i++) {
//      double f = i*0.1;
//      result = mb.invoke("base", Double.toString(f), "10000", "1000");
//      System.out.println("Record:");
//      System.out.println("{\"run\" : " + f + ", \"time\" : " + result + "}");
//    }
  }
}
