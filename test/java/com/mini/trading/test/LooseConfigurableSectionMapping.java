package com.mini.trading.test;

import java.util.List;

import com.ohua.engine.RuntimeProcessConfiguration;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.multithreading.Section;
import com.ohua.system.sections.mappers.ConfigurableSectionMapper;

/**
 * This mapper will map all unmapped operators to single sections.
 * 
 * @author sertel
 *
 */
// TODO migrate this into Ohua
public class LooseConfigurableSectionMapping extends ConfigurableSectionMapper {
  
  public LooseConfigurableSectionMapping(RuntimeProcessConfiguration config) {
    super(config);
  }
  
  protected List<Section> createSections(FlowGraph graphToConvert, List<List<String>> allSections) {
    // create the specified sections
    List<Section> specified = super.createSections(graphToConvert, allSections);
    // map every unmapped operator to its own section
    for(OperatorCore op : graphToConvert.getContainedGraphNodes()) {
      if(!isMapped(op, specified)) {
        specified.add(super.createSingleOpSection(op));
      }
    }
    return specified;
  }
  
  private boolean isMapped(OperatorCore op, List<Section> specified) {
    for(Section s : specified)
      if(s.getOperators().contains(op)) return true;
    return false;
  }
  
  /**
   * 
   * @param section
   * @param suspects
   * @return false whenever one of the suspects is outside of the section
   */
  protected boolean belongsToSection(List<OperatorCore> section, List<OperatorCore> suspects) {
    for(OperatorCore op : suspects) {
      if(!section.contains(op)) return false;
    }
    return true;
  }

}
