package com.mini.trading.test;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import com.ohua.lang.compile.FlowGraphCompiler;

import clojure.java.api.Clojure;
import clojure.lang.IFn;
import aua.analysis.reference.ReturnArrayReferenceAnalysis;

public class testOhuaCompilation {
  
  public static class ArrayListAdd {
    public Object[] go() {
      ArrayList<String> l = new ArrayList<>();
      l.add("value");
      return new Object[] { l };
    }
  }
  
  public static class HashMapPut {
    public Object[] go() {
      HashMap<String, String> m = new HashMap<>();
      m.put("key", "value");
      return new Object[] { m };
    }
  }
  
  @Test
  public void compileArrayList() throws Throwable {
    arrayList(false);
  }
  
  public static void arrayList(boolean parallel) throws Throwable {
    ReturnArrayReferenceAnalysis.PARALLEL = parallel;
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    long start = System.currentTimeMillis();
    analysis.analyze(ArrayListAdd.class.getName(), "go", Object[].class);
    long time = (System.currentTimeMillis() - start);
    System.gc();
    System.gc();
    System.gc();
    Runtime rt = Runtime.getRuntime();
    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
    System.out.println("{ \"lib\" : \"ohua\", \"time(ms)\" : " + time + ", \"space(MB)\" : " + usedMB
                       + ", \"type\" : \"array-list\", \"parallel\" : " + parallel + " }");
  }
  
  @Test
  public void compileHashMap() throws Throwable {
    hashMap(false);
  }
  
  public static void hashMap(boolean parallel) throws Throwable {
    ReturnArrayReferenceAnalysis.PARALLEL = parallel;
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    long start = System.currentTimeMillis();
    analysis.analyze(HashMapPut.class.getName(), "go", Object[].class);
    long time = (System.currentTimeMillis() - start);
    System.gc();
    System.gc();
    System.gc();
    Runtime rt = Runtime.getRuntime();
    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
    System.out.println("{ \"lib\" : \"ohua\", \"time(ms)\" : " + time + ", \"space(MB)\" : " + usedMB
                       + ", \"type\" : \"hash-map\", \"parallel\" : " + parallel + " }");
  }
  
  public static void footprint() throws Throwable {
    @SuppressWarnings("unused")
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    long start = System.currentTimeMillis();
    // HashMap<String, String> hm = new HashMap<>();
    // hm.put("k", "v");
    // analysis.analyze(HashMapPut.class.getName(), "go", Object[].class);
    long time = (System.currentTimeMillis() - start);
    System.gc();
    System.gc();
    System.gc();
    Runtime rt = Runtime.getRuntime();
    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
    System.out.println("{ \"lib\" : \"ohua\", \"time(ms)\" : " + time + ", \"space(MB)\" : " + usedMB + "}");
  }
  
  @Test
  public void compileProvider() throws Throwable{
    provider(false);
  }
  
  public static void provider(boolean parallel) throws Throwable {
    ReturnArrayReferenceAnalysis.PARALLEL = parallel;
    FlowGraphCompiler.PARALLEL_COMPILE = parallel;
    IFn require = Clojure.var("clojure.core", "require");
    long start = System.currentTimeMillis();
    require.invoke(Clojure.read("mini.trading.provider"));
    // load the provider function
    Clojure.var("mini.trading.provider", "provide");
    long time = (System.currentTimeMillis() - start);
    System.gc();
    System.gc();
    System.gc();
    Runtime rt = Runtime.getRuntime();
    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
    System.out.println("{ \"lib\" : \"ohua\", \"time(ms)\" : " + time + ", \"space(MB)\" : " + usedMB + "}");
  }

  @Test
  public void compileTrading() throws Throwable{
    trading(false);
  }
  
  public static void trading(boolean parallel) throws Throwable {
    ReturnArrayReferenceAnalysis.PARALLEL = parallel;
    FlowGraphCompiler.PARALLEL_COMPILE = parallel;

    IFn require = Clojure.var("clojure.core", "require");
    long start = System.currentTimeMillis();
    require.invoke(Clojure.read("mini.trading.trader"));
    // load the trader function
    Clojure.var("mini.trading.trader", "trading-node");
    long time = (System.currentTimeMillis() - start);
    System.gc();
    System.gc();
    System.gc();
    Runtime rt = Runtime.getRuntime();
    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
    System.out.println("{ \"lib\" : \"ohua\", \"time(ms)\" : " + time + ", \"space(MB)\" : " + usedMB + "}");
  }

  @Test
  public void compileRequester() throws Throwable{
    requester(false);
  }
  
  public static void requester(boolean parallel) throws Throwable {
    ReturnArrayReferenceAnalysis.PARALLEL = parallel;
    FlowGraphCompiler.PARALLEL_COMPILE = parallel;

    IFn require = Clojure.var("clojure.core", "require");
    long start = System.currentTimeMillis();
    require.invoke(Clojure.read("mini.trading.requester"));
    // load the provider function
    Clojure.var("mini.trading.requester", "request");
    long time = (System.currentTimeMillis() - start);
    System.gc();
    System.gc();
    System.gc();
    Runtime rt = Runtime.getRuntime();
    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
    System.out.println("{ \"lib\" : \"ohua\", \"time(ms)\" : " + time + ", \"space(MB)\" : " + usedMB + "}");
  }

  /**
   * Perform experiments from this hook. Execute via:<br>
   * lein run -m com.mini.trading.test.testOhuaCompilation
   * 
   * @param args
   * @throws Throwable
   */
  public static void main(String[] args) throws Throwable {
    System.out.println("------------------------------------------------------------");
    System.out.println("Running compilation for '" + args[0] + "' :");
    switch(args[0]) {
      case "p":
        provider(false);
        break;
       case "p-parallel":
       provider(true);
       break;
       case "r":
       requester(false);
       break;
       case "r-parallel":
       requester(true);
       break;
       case "t":
       trading(false);
       break;
       case "t-parallel":
       trading(true);
       break;
      case "al":
        arrayList(false);
        break;
      case "al-parallel":
        arrayList(true);
        break;
      case "hm":
        hashMap(false);
        break;
      case "hm-parallel":
        hashMap(true);
        break;
      case "fp":
        footprint();
        break;
      default:
        System.out.println("Unknown option: " + args[0]);
    }
    System.out.println("------------------------------------------------------------");
  }
}
