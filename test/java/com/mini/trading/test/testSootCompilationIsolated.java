package com.mini.trading.test;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

public class testSootCompilationIsolated {
  
  public static class ArrayListAdd {
    public Object[] go() {
      ArrayList<String> l = new ArrayList<>();
      l.add("value");
      return new Object[] { l };
    }
  }
  
  public static class HashMapPut {
    public Object[] go() {
      HashMap<String, String> m = new HashMap<>();
      m.put("key", "value");
      return new Object[] { m };
    }
  }
  
  @Test
  public void compileArrayList() throws Throwable {
    arrayList(true);
  }
  
  public static void arrayList(boolean geo) throws Throwable {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize(geo);
    long start = System.currentTimeMillis();
    analysis.analyze(ArrayListAdd.class.getMethod("go"));
    // analysis.analyze(ArrayList.class.getMethod("add", Object.class));
    long time = (System.currentTimeMillis() - start);
    System.gc();
    System.gc();
    System.gc();
    Runtime rt = Runtime.getRuntime();
    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
    System.out.println("{ \"lib\" : \"soot\", \"time(ms)\" : " + time + ", \"space(MB)\" : " + usedMB
                       + ", \"type\" : \"trading\", \"geo-enabled\" : " + geo + " }");
  }
  
  @Test
  public void compileHashMap() throws Throwable {
    hashMap(false);
  }
  
  public static void hashMap(boolean geo) throws Throwable {
    SootReferenceAnalysis analysis = new SootReferenceAnalysis();
    analysis.initialize(geo);
    long start = System.currentTimeMillis();
    // analysis.analyze(HashMap.class.getMethod("put", Object.class, Object.class));
    analysis.analyze(HashMapPut.class.getMethod("go"));
    long time = (System.currentTimeMillis() - start);
    System.gc();
    System.gc();
    System.gc();
    Runtime rt = Runtime.getRuntime();
    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
    System.out.println("{ \"lib\" : \"soot\", \"time(ms)\" : " + time + ", \"space(MB)\" : " + usedMB
                       + ", \"type\" : \"trading\", \"geo-enabled\" : " + geo + " }");
  }
  
  /**
   * Perform experiments from this hook. Execute via:<br>
   * lein run -m com.mini.trading.test.testSootCompilationIsolated
   * 
   * @param args
   * @throws Throwable
   */
  public static void main(String[] args) throws Throwable {
    System.out.println("------------------------------------------------------------");
    System.out.println("Running compilation for '" + args[0] + "' :");
    switch(args[0]) {
      case "al":
        arrayList(false);
        break;
      case "al-geo":
        arrayList(true);
        break;
      case "hm":
        hashMap(false);
        break;
      case "hm-geo":
        hashMap(true);
        break;
      default:
        System.out.println("Unknown option: " + args[0]);
    }
    System.out.println("------------------------------------------------------------");
  }
}
