package com.mini.trading.test;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import com.mini.trading.messages.NewSingleOrder;
import com.mini.trading.operators.AcceptOperator;
import com.mini.trading.operators.CloseOperator;
import com.mini.trading.operators.ConnectOperator;
import com.mini.trading.operators.LogFunctionality.Logger;
import com.mini.trading.operators.LogFunctionality.OrderLogEntryBuilder;
import com.mini.trading.operators.LogFunctionality.PriceInfoLogEntryBuilder;
import com.mini.trading.operators.LogFunctionality.ReportLogEntryBuilder;
import com.mini.trading.operators.LogFunctionality.RfsLogEntryBuilder;
import com.mini.trading.operators.PipeOperator;
import com.mini.trading.operators.ReadOperator;
import com.mini.trading.operators.RequestHandling.RFSRequestParser;
import com.mini.trading.operators.RequestHandling.RequestTypeParser;
import com.mini.trading.operators.SendOperator;
import com.mini.trading.operators.trader.ConnectToProvidersOperator;
import com.mini.trading.operators.trader.LogEntryBuilder.AdminRequestLogEntryBuilder;
import com.mini.trading.operators.trader.OrderArgsOperator.OrderMessageBuilder;
import com.mini.trading.operators.trader.OrderArgsOperator.ProviderRegistryAccess;
import com.mini.trading.operators.trader.RFSFunctionality.RFSMessageBuilder;
import com.mini.trading.operators.trader.RegisterOperator;
import com.mini.trading.operators.trader.SetTradingOperator;
import com.mini.trading.operators.trader.TraderRequestHandling.AdminRequestParser;
import com.mini.trading.operators.trader.TraderRequestHandling.OrderRequestParser;
import com.mini.trading.operators.trader.TraderRequestHandling.PriceResponseParser;
import com.mini.trading.operators.trader.TraderRequestHandling.RegisterRequestParser;

/**
 * Created just for experimental purposes.
 * 
 * @author sertel
 *
 */
public class TradingPlatformJavaAlgo {
  
  /**
   * (let<br>
   * ; receive requests<br>
   * [[req conn-to-req] (read (accept server))<br>
   * [type content] (parse-request-type req)]<br>
   * (cond<br>
   * <br>
   * ; en- /disable trading, requested by the admin<br>
   * (= "admin" type) (let [cnn (pipe conn-to-req)] <br>
   * (log-entry (build-admin-log-entry (set-trading (parse-admin-request content) trading)) <br>
   * configs log-it)<br>
   * (close cnn))<br>
   * <br>
   * ; register a new provider<br>
   * (= "register" type) (let [prov (parse-register-request content)<br>
   * cnn (pipe conn-to-req)]<br>
   * (register prov provs)<br>
   * (close cnn))<br>
   * <br>
   * ; forward the received rfs to the registered provider and send the according offers to the
   * requester<br>
   * (= "rfs" type) (let [[provider id time product requester] (parse-rfs-request content)<br>
   * new-conn-to-req (pipe conn-to-req)]<br>
   * (log-entry (build-rfs-log-entry id provider time product requester) requests log-it)<br>
   * (let [provider-info (get-provider provider provs)]<br>
   * ; TODO send negative report: FIX System Messages -> Reject<br>
   * (if (= provider-info nil) (close new-conn-to-req) <br>
   * (let [requester-cnn (pipe new-conn-to-req)<br>
   * [price-msg passed-conn] (read <br>
   * (send <br>
   * (build-rfs-msg id time product requester) <br>
   * (connect-to-prov provider-info)))<br>
   * price (parse-price-info price-msg)]<br>
   * (log-entry (build-price-log-entry price) prices log-it)<br>
   * (close passed-conn)<br>
   * (close (send price-msg requester-cnn))))))<br>
   * <br>
   * ; forward the received order to the provider and send the report to the requester<br>
   * (and (= "order" type)) ;(@trading))<br>
   * (let [[new-conn-to-req] (pipe conn-to-req)<br>
   * [provider order] (parse-order-request content)]<br>
   * (log-entry (build-order-log-entry order) orders log-it)<br>
   * (let [[addr port] (get-provider provider provs)<br>
   * [report conn-to-prov] (read (send (build-order-msg order) (connect addr port)))]<br>
   * (log-entry (build-report-log-entry report) reports log-it)<br>
   * (close conn-to-prov)<br>
   * (close (send report new-conn-to-req)))<br>
   */
  @SuppressWarnings("unchecked")
  public void run(ServerSocket socket, Map<String, String[]> providers) {
    AtomicReference<Boolean> trading = new AtomicReference<Boolean>(true);
    ArrayList<String> configs = new ArrayList<>();
    ArrayList<String> requests = new ArrayList<>();
    ArrayList<String> prices = new ArrayList<>();
    ArrayList<String> orders = new ArrayList<>();
    ArrayList<String> reports = new ArrayList<>();
    boolean logIt = true;
    
    Object[] readOutput = new ReadOperator().read((Socket) new AcceptOperator().accept(socket)[0]);
    Object[] parseOutput = new RequestTypeParser().parseRequestType((List<String>) readOutput[0]);
    switch((String) parseOutput[0]) {
      case "admin":
        Object[] tradingOutput =
            new SetTradingOperator().setTrading((boolean) new AdminRequestParser().parseAdminRequest((List<String>) parseOutput[1])[0],
                                                trading);
        Object[] adminLogEntry = new AdminRequestLogEntryBuilder().buildAdminLogEntry((boolean) tradingOutput[0]);
        new Logger().logEntry((String) adminLogEntry[0], configs, logIt);
        new CloseOperator().close((Socket) new PipeOperator().pipe(readOutput[0])[0]);
        break;
      case "register":
        Object[] parsedRegister = new RegisterRequestParser().parseRegisterRequest((List<String>) readOutput[0]);
        new RegisterOperator().register((String) parsedRegister[0],
                                        (String) parsedRegister[1],
                                        (String) parsedRegister[2],
                                        providers);
        new CloseOperator().close((Socket) new PipeOperator().pipe(readOutput[0])[0]);
        break;
      case "rfs":
        Object[] parsedRFS = new RFSRequestParser().parseRfsRequest((List<String>) readOutput[0]);
        new Logger().logEntry((String) new RfsLogEntryBuilder().buildRfsLogEntry((String) parsedRFS[1],
                                                                                 (String) parsedRFS[0],
                                                                                 (String) parsedRFS[2],
                                                                                 (String) parsedRFS[3],
                                                                                 (String) parsedRFS[4])[0],
                              requests,
                              logIt);
        Object[] providerInfo = new ProviderRegistryAccess().getProvider((String) parsedRFS[0], providers);
        Object[] passedCnn = new PipeOperator().pipe(readOutput[0]);
        if(providerInfo[0] == null) {
          new CloseOperator().close((Socket) passedCnn[0]);
        } else {
          /**
           * * (let [requester-cnn (pipe new-conn-to-req)<br>
           * [price-msg passed-conn] (read <br>
           * (send <br>
           * (build-rfs-msg id time product requester) <br>
           * (connect-to-prov provider-info)))<br>
           * price (parse-price-info price-msg)]<br>
           * (log-entry (build-price-log-entry price) prices log-it)<br>
           * (close passed-conn)<br>
           * (close (send price-msg requester-cnn))))))<br>
           */
          // (let [[provider id time product requester] (parse-rfs-request content)<br>
          Object[] providerRead =
              new ReadOperator().read((Socket) new SendOperator().send((List<String>) new RFSMessageBuilder().buildRfsMsg((String) parsedRFS[1],
                                                                                                                          (String) parsedRFS[2],
                                                                                                                          (String) parsedRFS[3],
                                                                                                                          (String) parsedRFS[4])[0],
                                                                       (Socket) new ConnectToProvidersOperator().connectToProv((String) providerInfo[0],
                                                                                                                               (String) providerInfo[1])[0])[0]);
          Object[] price = new PriceResponseParser().parsePriceInfo((List<String>) providerRead[0]);
          new Logger().logEntry((String) new PriceInfoLogEntryBuilder().buildPriceLogEntry((String) price[0],
                                                                                           (String) price[1],
                                                                                           (String) price[2],
                                                                                           (String) price[3],
                                                                                           (String) price[4],
                                                                                           (String) price[5])[0],
                                prices,
                                logIt);
          new CloseOperator().close((Socket) providerRead[1]);
          new CloseOperator().close((Socket) new SendOperator().send((List<String>) providerRead[0],
                                                                     (Socket) new PipeOperator().pipe(readOutput[0])[0])[0]);
        }
        
        break;
      case "order":
        /**
         * (let [[new-conn-to-req] (pipe conn-to-req)<br>
         * [provider order] (parse-order-request content)]<br>
         * (log-entry (build-order-log-entry order) orders log-it)<br>
         * (let [[addr port] (get-provider provider provs)<br>
         * [report conn-to-prov] (read (send (build-order-msg order) (connect addr port)))]<br>
         * (log-entry (build-report-log-entry report) reports log-it)<br>
         * (close conn-to-prov)<br>
         * (close (send report new-conn-to-req)))<br>
         */
        Object[] parsedOrder = new OrderRequestParser().parseOrderRequest((List<String>) readOutput[0]);
        new Logger().logEntry((String) new OrderLogEntryBuilder().buildOrderLogEntry((Map<?, ?>) parsedOrder[1])[0],
                              orders,
                              logIt);
        Object[] providerInf = new ProviderRegistryAccess().getProvider((String) parsedOrder[0], providers);
        Object[] readResp = new ReadOperator().read((Socket) new SendOperator().send((List<String>) new OrderMessageBuilder().buildOrderMsg((Map<NewSingleOrder, ?>) parsedOrder[1])[0],
                                                                 (Socket) new ConnectOperator().connect((String) providerInf[0],
                                                                                                        (String) providerInf[1])[0])[0]);
        new Logger().logEntry((String) new ReportLogEntryBuilder().buildReportLogEntry((List<String>) readResp[0])[0], reports, logIt);
        new CloseOperator().close((Socket) readResp[1]);
        new CloseOperator().close((Socket) new SendOperator().send((List<String>) readResp[0],
                                                                   (Socket) new PipeOperator().pipe(readOutput[0])[0])[0]);
        break;
      default:
        // impossible
        return;
    }
  }
}
