/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.mini.trading.test;

import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.Properties;

import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.junit.Test;

import com.mini.trading.test.HttpWebServerExperiment.Client;
import com.mini.trading.test.HttpWebServerExperiment.Experiment;
import com.mini.trading.test.HttpWebServerExperiment.Request;
import com.mini.trading.test.HttpWebServerExperiment.RequestType;

public class TraderExperiment {
  
  public static class OrderRequest extends Request {
    int _providerId = -1;
    
    OrderRequest(int provider) {
      _providerId = provider;
    }
    
    // protected String writeRequest(PrintWriter writer, String resource, String address){
    // writer.println("type=order");
    // writer.println("id=5");
    // long timeInMillis = GregorianCalendar.getInstance().getTimeInMillis();
    // Timestamp time = new Timestamp(timeInMillis);
    // writer.println("time=" + time);
    // writer.println("rfsid=500");
    // writer.println("product=silver");
    // writer.println("isrequesterbuying=true");
    // writer.println("pricelimit=5000");
    // writer.println("requester=requester-A");
    // writer.println("provider=provider-" + _providerId);
    // writer.println("");
    //
    // return "type=report";
    // }
    
    protected String writeRequest(PrintWriter writer, String resource, String address) {
      writer.println("type=order");
      writer.println("0=provider-" + _providerId);
      writer.println("1=order-500");
      writer.println("2=quote-500");
      writer.println("3=test-account");
      writer.println("4=test-user");
      long timeInMillis = GregorianCalendar.getInstance().getTimeInMillis();
      Timestamp time = new Timestamp(timeInMillis);
      writer.println("5=" + time);
      writer.println("6=FX-STD");
      writer.println("7=true");
      writer.println("8=5000.00");
      
      // Too large messages turn the parser into a bottleneck and forces us to talk about data
      // parallelism which we do not want.
      writer.println("9=3000.00");
      writer.println("10=USD/CHF");
      writer.println("11=USD");
      writer.println("12=20.21");
      writer.println("13=20.50");
      writer.println("14=1");
      writer.println("15=10000000");
      writer.println("16=some order restriction");
      writer.println("17=D");
      writer.println("18=" + time);
      
      writer.println("");
      return "type=report";
    }
  }
  
  /**
   * Adds a new entry to the provider list that is never queried. Hence, there will never be any
   * conflict.
   * 
   * @author sertel
   *
   */
  public static class RegisterRequest extends Request {
    int _providerId = -1;
    String[][] _providers = new String[][] {
                                            // local testing
                                            new String[] { "ohua.local",
                                                          "8094" },
                                            new String[] { "ohua.local",
                                                          "8095" }                                             };
    
    RegisterRequest(int provider) {
      _providerId = provider;
    }
    
    protected String writeRequest(PrintWriter writer, String resource, String address) {
      writer.println("type=register");
      writer.println("provider=provider-" + _providerId);
      writer.println("addr=" + _providers[_providerId][0]);
      writer.println("port=" + _providers[_providerId][1]);
      writer.println("");
      // no response expected
      return null;
    }
  }
  
  public static class RFSRequest extends Request {
    int _providerId = -1;
    
    RFSRequest(int provider) {
      _providerId = provider;
    }
    
    protected String writeRequest(PrintWriter writer, String resource, String address) {
      writer.println("type=rfs");
      writer.println("id=5");
      long timeInMillis = GregorianCalendar.getInstance().getTimeInMillis();
      Timestamp time = new Timestamp(timeInMillis);
      writer.println("time=" + time);
      writer.println("product=FX-STD");
      writer.println("requester=requester-A");
      writer.println("provider=provider-" + _providerId);
      writer.println("");
      return "type=price";
    }
  }
  
  public static class TradeClient extends Client {
    int _providerCount = 0;
    UniformIntegerDistribution _providerDistribution = null;
    String _requesterType = null;
    Request _lastRequest = new RegisterRequest(0);
    
    public TradeClient(String requesterType,
                       String serverAddress,
                       int port,
                       int startingPort,
                       int serverCount,
                       int upperFileBound,
                       String url)
    {
      super(serverAddress, port, startingPort, serverCount, upperFileBound, url, RequestType.GET, 512);
      _providerCount = upperFileBound;
      if(_providerCount > 0)
        _providerDistribution = new UniformIntegerDistribution(0, _providerCount);
      _requesterType = requesterType;
    }
    
    protected Request createRequest() {
      if(_requesterType.equals("order")) return new OrderRequest(getProviderDistributionSample());
      else if(_requesterType.equals("register")) return new RegisterRequest(getProviderDistributionSample());
      else if(_requesterType.equals("alternate")) {
        // FIXME alternate between put and gets for the provider
        _lastRequest =
            _lastRequest instanceof RFSRequest ? new RegisterRequest(getProviderDistributionSample()) : new RFSRequest(getProviderDistributionSample());
        return _lastRequest;
      } else return new RFSRequest(getProviderDistributionSample());
    }

    public int getProviderDistributionSample() {
      if(_providerDistribution == null) return 0;
      else return _providerDistribution.sample();
    }
  }
  
  public static class TradeExperiment extends Experiment {
    String _requesterType = null;
    
    public TradeExperiment(String requesterType) {
      _requesterType = requesterType;
    }
    
    protected Client createClient(String serverAddress, int port, int startingPort, int serverCount,
                                  int upperFileBound, String url, RequestType requestType, int contentSize)
    {
      return new TradeClient(_requesterType, serverAddress, port, startingPort, serverCount, upperFileBound, url);
    }
  }
  
  @Test
  public void experiment() throws Throwable {
    Properties props = Experiment.loadConfig("test-input/configuration.properties");
    String requesterType = props.getProperty("requester-type");
    Experiment e = new TradeExperiment(requesterType);
    long start = System.currentTimeMillis();
    e.execute("test-input/configuration.properties");
    System.out.println("Writing statistics");
    e.writeStatistics("test-output/statistics.txt", start);
  }
  
  public static void main(String[] args) throws Throwable{
    new TraderExperiment().experiment();
  }
}
