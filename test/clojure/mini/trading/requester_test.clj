(ns mini.trading.requester-test
  (:use [com.ohua.compile])
  (:require [clojure.test :refer :all]
            [mini.trading.requester :as req]))

(deftest single-requester-test [] 
  (println "Starting the request.")
  (req/request "127.0.0.1" 8090))