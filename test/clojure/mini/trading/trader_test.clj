(ns mini.trading.trader-test
  (:use [com.ohua.compile])
  (:require [clojure.test :refer :all]
            [mini.trading.trader :as tra]))

(deftest single-trade-test []
;  (println "Starting the trading node.")
  (tra/trading-node (tra/open-server 8090) 1))

(defn start
  ([] (start "1"))
  ([concurrent]
;  (println concurrent)
  (println "Starting the trading node.")
  (tra/trading-node (tra/open-server 8090) (read-string concurrent))
  ))
  