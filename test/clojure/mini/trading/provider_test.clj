(ns mini.trading.provider-test
  (:use [com.ohua.compile])
  (:require [clojure.test :refer :all]
            [clojure.string :only (join split)]
            [mini.trading.provider :as prov])
  (:import java.net.InetAddress java.util.HashMap com.mini.trading.operators.provider.ProviderFunctionality$ProductDetails))

(deftest single-provider-test
  (println "Starting the provider.")
  (let [server (prov/open-server 8094)]
    (prov/register "127.0.0.1" 8090 server "provider-0")
    (Thread/sleep 5000)
    (prov/provide server 
                  (doto 
                    (new java.util.HashMap) 
                    (.put "FX-STD"
                      (new com.mini.trading.operators.provider.ProviderFunctionality$ProductDetails 10 10.0 20.0))
                    (.put "MM" 
                      (new com.mini.trading.operators.provider.ProviderFunctionality$ProductDetails 11 15.0 25.0))
                    )
                  "provider-0")))
      
;;; The good thing is that this thing can have arguments.
(defn multiple-providers [trader-node trader-port local-id]
 (println "Starting the provider.")
 (println "Args:" trader-node trader-port)
 (let [;provider-id (str "provider-" (.getHostName (InetAddress/getLocalHost)) ":" local-id)
       provider-id (str "provider-" local-id)
       server (prov/open-server (+ 8094 (read-string local-id)))]
;                 (prov/register trader-node (read-string trader-port) server provider-id)
                 (Thread/sleep 2000)
                 (prov/provide server
                               ; TODO change this to support different types of maps!
                               (doto 
                                 (new java.util.HashMap) 
                                 (.put "FX-STD"
;                                        (.put "silver"
                                         (new com.mini.trading.operators.provider.ProviderFunctionality$ProductDetails 10 10.0 20.0))
                                        (.put "MM" 
;                                        (.put "gold"
                                          (new com.mini.trading.operators.provider.ProviderFunctionality$ProductDetails 11 15.0 25.0))
                                        ) 
                               provider-id))
 )

(defn update-provider 
  [address port]
  (println "Updating provider ...")
  (doto (new java.io.PrintWriter (new java.io.OutputStreamWriter (.getOutputStream (new java.net.Socket address (read-string port)))))
    (.println "type=put")
    (.println "NDS=10:10.02:20.05")
    (.println "")
    (.flush )
    (.close )))

(defn generate-rfq [product]
  (doto 
    (new java.util.ArrayList) 
    (.add "type=rfs") 
    (.add "id=5")
    (.add (clojure.string/join ["time=" (str (.getTimeInMillis (java.util.GregorianCalendar/getInstance)))]))
    (.add (clojure.string/join ["product=product-" (str product)]))
    (.add "requester=requester-A")
    (.add "provider=provider-0")))

(defn gen-rfq [product] 
  (to-array [(generate-rfq product) (new com.mini.trading.operators.provider.benchmark.Benchmarking$Output)]))

(defn generate-put [product]
  (doto (new java.util.ArrayList) (.add "type=put") (.add (clojure.string/join ["product-" (str product) "=10:10.0:20.0"]))))

(defn gen-put [product]
  (to-array [(generate-put product) (new com.mini.trading.operators.provider.benchmark.Benchmarking$Output)]))

(defn generate-workload [percent-reads request-total data-total]
  ; we model the distribution like so: P(x<percent-reads*100) = read, P(x>=percent-reads*100) = write on a uniform distribution over 100.
  (let [b (* percent-reads 100)
        dist (new org.apache.commons.math3.distribution.UniformIntegerDistribution 1 100)
        data-dist (new org.apache.commons.math3.distribution.UniformIntegerDistribution 1 data-total)]
    (map 
      (fn [_] (if (< (.sample dist) b) (gen-rfq (.sample data-dist)) (gen-put (.sample data-dist))))
      (range request-total))))

(defn gen-wl [p r d]
  (let [a (generate-workload (read-string p) (read-string r) (read-string d))]
;    (println a)
    a))

(defn prepare [data-total ds]
  (dosync
    (dotimes [n (read-string data-total)] (.put ds 
                                            (clojure.string/join ["product-" (str (inc n))]) 
                                            (new com.mini.trading.operators.provider.ProviderFunctionality$ProductDetails (int 10) (double 10.0) (double 20.0))))))

(defn micro-benchmark [type percent-reads total-requests total-data concurrent]
  ; generate some data as input
  (let [input (to-array (gen-wl percent-reads total-requests total-data))]
    ; generate some initial data
    (let [initial (cond (= type "base") (new java.util.HashMap)
                        (= type "concurrent") (new java.util.concurrent.ConcurrentHashMap)
;                        (= type "stm") (new com.ohua.lang.runtime.stm.STMHashMap)
                        :default (throw (Exception. (clojure.string/join "unknown data structure type:" type))))
          _ (prepare total-data initial)]
      (println "Starting benchmark ...")
      ; start the benchmark
      (let [start (System/currentTimeMillis)]
        (prov/provide nil initial "provider-0" input (read-string concurrent))
;        (println "done")
;        (- (System/currentTimeMillis) start)
         (println "{\"run\" :" percent-reads ", \"time\" :" (- (System/currentTimeMillis) start) "}")
         (java.lang.System/exit 0)
;        (println "Benchmark finished with output: ") 
;      (println (map (fn [in] (._output (nth in 1))) input))
  ))))