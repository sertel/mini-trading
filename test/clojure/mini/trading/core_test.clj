(ns mini.trading.core-test
  (:use [com.ohua.compile])
  (:require [clojure.test :refer :all]
            [mini.trading.provider :as prov]
            [mini.trading.requester :as req]
            [mini.trading.trader :as tra]
            )
  )

(deftest single-trade-test
  
  (let [trade-server (tra/open-server 8090)
        provider-server (prov/open-server 8094)]
    (def t (future (do
                     (println "Starting the trading node.")
                     (tra/trading-node trade-server))))
    (def p (future (do 
                     (println "Starting the provider.")
                     (Thread/sleep 5000)
                     (prov/register "127.0.0.1" 8090 provider-server)
                     (prov/provide provider-server))))
    (def r (future (do 
                     (println "Starting the request.")
                     (Thread/sleep 10000)
                     (req/request)))) 
    
    (println (deref r 60000 "\nApparently the test trade doesn't return.")) ;TODO adjust the timeout
    
    (clojure.test/is (= (count @req/requests) 1))
    (clojure.test/is (= (count @req/prices) 1))
    (clojure.test/is (= (count @req/reports) 1))
    
    (clojure.test/is (= @req/requests @tra/requests))
    (clojure.test/is (= @req/requests @prov/requests))
  
    (clojure.test/is (= @req/prices @tra/prices))
    (clojure.test/is (= @req/prices @prov/prices))
  
    (clojure.test/is (= @req/orders @tra/orders))
    (clojure.test/is (= @req/orders @prov/orders))
  
    (clojure.test/is (= @req/reports @tra/reports))
    (clojure.test/is (= @req/reports @prov/reports))
    
  ))

