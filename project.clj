(defproject mini.trading "0.1.0-SNAPSHOT"
  :description "An online trading platform implemented with Ohua."
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :plugins [[lein-junit "1.1.8"]
            ; plugin needed for META-INF handling
            [lein-javac-resources "0.1.1"]]
  :dependencies [[org.clojure/clojure "1.6.0"]
                 ;                [ohua/ohua "0.2.2"]
                 [com.esotericsoftware/kryo "3.0.0" :exclusions [[org.ow2.asm/asm :extension "jar"]]]
                 [org.apache.commons/commons-math3 "3.2"]
              
                 [junit/junit "4.12"]]

;  :main mini.trading.core
  :source-paths ["src/clojure"]
  :java-source-paths ["src/java" "test/java"]
  :test-paths ["test/clojure"]
  ; the below ends up on the class path (before the dependencies!)
  :resource-paths ["resources/lib/quickfixj-all-1.5.3.jar"
                   ; AUA patch
                   "resources/lib/aua-0.1.3-SNAPSHOT.jar"
                   ; Ohua patch
                   "resources/lib/ohua-0.2.4-SNAPSHOT-standalone.jar"
                   ; Soot lib
                   "lib/soot-nightly-14012015/soot-trunk.jar"
                   ]
  
  ; lein-junit configuration
  :junit ["test/java"]
  :junit-test-file-pattern #".*\/testBenchmark.java"
  :junit-formatter :brief
  :junit-results-dir "test-output-lein"

  ; this is important in order to produce enough information for our analysis to work properly.
  ; the -g option is important because it includes the local variable table into the byte code
  ; which we use to assign names to parameters.
;  :java-cmd "/Library/Java/JavaVirtualMachines/jdk1.7.0_40.jdk/Contents/Home/bin/java"
;  :javac-options ["-target" "1.7" "-source" "1.7" "-Xlint:-options" "-g"]
  :javac-options ["-target" "1.8" "-source" "1.8" "-Xlint:-options" "-g"]
  :jvm-opts ["-Xmx4g" 
;             "-XX:+UseConcMarkSweepGC"
;             "-Djava.util.logging.config.file=dev-resources/consoleLogging.properties"
;             "-verbose:gc"
;             "-Xloggc:gc.log"
;             "-XX:+PrintGCTimeStamps"
;             "-XX:+PrintGC"
;             "-XX:+PrintGCDetails"
;             "-XX:+PrintGCApplicationConcurrentTime"
;             "-XX:+PrintGCApplicationStoppedTime"
             ]
  
    ; this is needed to handle META-INF directories properly (copy them to :compile-path) 
  :hooks [leiningen.javac-resources]
  ; with the extension above we must explicitly exclude java source files!
  :jar-exclusions [#"\.java$"]
)
