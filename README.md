# mini.trading

This is a prototype of an online trading system implemented with [Ohua](https://bitbucket.org/sertel/ohua), an implementation of the stateful functional programming model.
The online trading system composes out of 3 components:

- A Trader (requester) that wishes to trade assets.
- A Provider (provider) that provides asset for trading.
- A Trading (trader) platform that connects traders and providers to establish a platform for trading.

## Running the components and benchmarks

All execution can be ignited via [leiningen](http://leiningen.org). You will have to first start the trading platform and afterwards any number of providers can be reqistered. Trading may start as soon as the trading platform was initiated and is ready to accept connections.

Please start the trading platform and a provider like so:
```
lein run -m mini.trading.trader-test/start
lein run -m mini.trading.provider-test/single-provider-test
```

A trader (requester) experiment can be executed as follows:
```
lein run -m com.mini.trading.test.TraderExperiment
```
Please have a look at the ```test-input/configuration.properties``` file to understand available benchmark properties.

If you are interested in the dataflow graphs that get generated as a runtime representation of the algorithms from the provider, trader and requester then have a look at the dotty files in the```test`` folder. You can view them with [Graphviz](http://www.graphviz.org/).

## Running compilation benchmarks

This prototype incorporates various tests and experiments for compilation as well as for runtime. You can find them in the test folders accordingly.
Once more, the best way to run those test is via leiningen.

You may run the compilation experiments like so:

```
lein run -m com.mini.trading.test.testOhuaCompilation <option>
lein run -m com.mini.trading.test.testSootCompilation <option>
```

Valid options for Soot are ```p t r p-geo t-geo r-geo```.
Valid options for Ohua are ```p t r p-parallel t-parallel r-parallel```.

Please make sure that you run on Java 1.7 for [Soot](http://sable.github.io/soot/) and 1.8 for Ohua. You configure this option in project.clj. 

Note, there also exist JUnit test cases to run all experiments conveniently in your favorite IDE (eclipse etc.). 
